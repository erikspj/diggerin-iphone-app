/*
 DAPI iOS Library
 
 See http://dapi.diggerin.com/
 
 Copyright (C) 2015 DiggerIn
 
 Automatically generated from DAPI source-code 2015-02-26 10:19
 
 Licensing terms not decided as of February 2015 (A permissive license is most probable).
 Please enquire as necessary to bef_diggerin@diggerin.com
 */
#import <Foundation/Foundation.h>

@interface DAPI : NSObject

+(void)setServerAddress:(NSString*)serverAddress;
+(NSString*)getServerAddress;




/**
 * DAPI method Summary
 *
 * Returns a HTML summary for the person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Summary.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Summary/{id}/
 *
 * Returns a HTML summary for the person identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the person identified by the authorization credentials used.
 *
 * {id} may be any of
 * dapi_person_id,
 * email,
 * person_name,
 * person_group_name
 *
 *
 * If {id} identifies more than one person an error response is generated
 * The response will always be in HTML-format (with Javascript). JSON will not be returned.
 */
+(NSMutableURLRequest*) getSummaryId: (NSString*)id;




/**
 * DAPI method VerifyCredentials
 *
 * Verifies that the credentials you supply for authorization correspond to a person in the database. <br>
 *
 * http://dapi.diggerin.com/documentation/VerifyCredentials.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: VerifyCredentials
 * Syntax: VerifyCredentials/{client_id}/
 *
 * Verifies that the credentials you supply for authorization correspond to a person in the database.
 * Note that the credentials in themselves are not supplied as parameters to this method as authorization is done "outside" of DAPI (as HTTP Basic authentication)
 *
 * Returnes scalar_result true if the credentials are correct
 *
 * If the credentials are not correct the response will be an error message from the authorization mechanism used,
 * this will not be in the standard DAPI-format, that is dapi_status, scalar_result or similar will NOT be defined.
 *
 * This method is somewhat more reliable than just doing an API-call and checking that no HTTP 4xx error code was returned, since
 * it checks for the affirmative instead of the negative. It is recommended that the client uses this method whenever the client asks the
 * user for credentials (that is, whenever the client asks the user for login-information).
 *
 * The value for the optional parameter client_id should correspond to one of the ids given by -Client/Versions-
 * with version-number added (for instance like DiggerIn_Android_vTEST). client_id is then used to generate statistics about clients in use
 *
 *
 * See also http://dapi.diggerin.com/documentation/VerifyPassword.html for which credentials ARE included as parameters
 *
 */
+(NSMutableURLRequest*) getVerifyCredentials;


/**
 * DAPI method VerifyCredentials
 *
 * Verifies that the credentials you supply for authorization correspond to a person in the database. <br>
 *
 * http://dapi.diggerin.com/documentation/VerifyCredentials.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: VerifyCredentials
 * Syntax: VerifyCredentials/{client_id}/
 *
 * Verifies that the credentials you supply for authorization correspond to a person in the database.
 * Note that the credentials in themselves are not supplied as parameters to this method as authorization is done "outside" of DAPI (as HTTP Basic authentication)
 *
 * Returnes scalar_result true if the credentials are correct
 *
 * If the credentials are not correct the response will be an error message from the authorization mechanism used,
 * this will not be in the standard DAPI-format, that is dapi_status, scalar_result or similar will NOT be defined.
 *
 * This method is somewhat more reliable than just doing an API-call and checking that no HTTP 4xx error code was returned, since
 * it checks for the affirmative instead of the negative. It is recommended that the client uses this method whenever the client asks the
 * user for credentials (that is, whenever the client asks the user for login-information).
 *
 * The value for the optional parameter client_id should correspond to one of the ids given by -Client/Versions-
 * with version-number added (for instance like DiggerIn_Android_vTEST). client_id is then used to generate statistics about clients in use
 *
 *
 * See also http://dapi.diggerin.com/documentation/VerifyPassword.html for which credentials ARE included as parameters
 *
 */
+(NSMutableURLRequest*) getVerifyCredentialsClient_id: (NSString*)client_id;




/**
 * DAPI method VerifyPassword
 *
 * Verifies that the email and password supplied are valid as credentials.<br>
 *
 * http://dapi.diggerin.com/documentation/VerifyPassword.html
 *
 * No authorization required HTTP methods allowed: GET, POST
 *
 * Syntax: VerifyPassword/{email}/{password}/
 *
 * Verifies that the email and password supplied are valid as credentials.
 *
 * Only recommended to use from Javascript-applications. In other situations it is recommended to use http://dapi.diggerin.com/documentation/VerifyCredentials.html.
 * Returns one of the following access_granted values:
 * NoAccess
 * FullAccess
 *
 *
 * See also http://dapi.diggerin.com/documentation/VerifyCredentials.html for which credentials are NOT included as parameters (http://dapi.diggerin.com/documentation/VerifyCredentials.html is the recommended method to use from a smartphone app)
 *
 */
+(NSMutableURLRequest*) getVerifyPasswordEmail: (NSString*)email password: (NSString*)password;

+(NSMutableURLRequest*) postVerifyPasswordEmail: (NSString*)email password: (NSString*)password;




/**
 * DAPI method ChangePassword
 *
 * Changes password<br>
 *
 * http://dapi.diggerin.com/documentation/ChangePassword.html
 *
 * No authorization required HTTP methods allowed: GET, POST
 *
 * Syntax: ChangePassword/{email}/{old_password}/{new_password}/
 *
 * Changes password
 *
 * This call is functionally equivalent to http://dapi.diggerin.com/documentation/PersonAddProperty.html with property_name set to "password" except that authorization is done with the parameters {email} and {old_password} instead of through the standard DAPI authorization mechanism (HTTP Basic Authorization).
 *
 * This makes it a bit easier to write a corresponding Javascript client for changing passwords in situations where you want to avoid the web-browser asking the user for credentials for authorization against DAPI.
 *
 * If the credentials are not correct then dapi_status will be set to "access_error"
 *
 * See also http://dapi.diggerin.com/documentation/ResetPassword.html
 */
+(NSMutableURLRequest*) getChangePasswordEmail: (NSString*)email old_password: (NSString*)old_password new_password: (NSString*)new_password;

+(NSMutableURLRequest*) postChangePasswordEmail: (NSString*)email old_password: (NSString*)old_password new_password: (NSString*)new_password;




/**
 * DAPI method ResetPassword
 *
 * Resetting of password<br>
 *
 * http://dapi.diggerin.com/documentation/ResetPassword.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: ResetPassword/{email}/
 * Syntax: ResetPassword/{email}/{execute_id}/
 *
 * Resetting of password
 *
 * The process consist of two steps:
 * Step 1: Request that an execute_id be created and sent as part of a step 2 URL-command to the specified email-address.
 * Step 2: Access the URL-command given in step 1.
 * In step 2 the current password for the person will be deactivated and the request will be redirected to UNFINISHED
 * from which the user may repeat the original registration-process.
 *
 *
 * See also http://dapi.diggerin.com/documentation/ChangePassword.html
 */
+(NSMutableURLRequest*) getResetPasswordEmail: (NSString*)email;


/**
 * DAPI method ResetPassword
 *
 * Resetting of password<br>
 *
 * http://dapi.diggerin.com/documentation/ResetPassword.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: ResetPassword/{email}/
 * Syntax: ResetPassword/{email}/{execute_id}/
 *
 * Resetting of password
 *
 * The process consist of two steps:
 * Step 1: Request that an execute_id be created and sent as part of a step 2 URL-command to the specified email-address.
 * Step 2: Access the URL-command given in step 1.
 * In step 2 the current password for the person will be deactivated and the request will be redirected to UNFINISHED
 * from which the user may repeat the original registration-process.
 *
 *
 * See also http://dapi.diggerin.com/documentation/ChangePassword.html
 */
+(NSMutableURLRequest*) getResetPasswordEmail: (NSString*)email execute_id: (NSString*)execute_id;




/**
 * DAPI method Translations/Version
 *
 * Gets version id of translations for the given client_id and language.<br>
 *
 * http://dapi.diggerin.com/documentation/TranslationsVersion.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: Translations/Version/{client_id}/{language}/
 *
 * Gets version id of translations for the given client_id and language.
 *
 * If the language is not recognized then information about 'en-US' is returned (American English).
 * Usually translations are cached by the client.
 * This call may be used by the client to verify that the locally cached translations at the client side are up to date.
 * This can be done by the client comparing the result of this call with the last value of 'version_id' returned by http://dapi.diggerin.com/documentation/Translations.html.
 *
 * Note that the version_id returned looks like a date and time but it is sufficient to just store it as a string and do string comparision against the value of 'version_id' returned by http://dapi.diggerin.com/documentation/Translations.html
 * If the version_id does not match then http://dapi.diggerin.com/documentation/Translations.html can be called in ordet to update the cache at the client side.
 *
 * See http://dapi.diggerin.com/documentation/Translations.html for more information about how to specify client_id and language.
 *
 */
+(NSMutableURLRequest*) getTranslationsVersionClient_id: (NSString*)client_id language: (NSString*)language;




/**
 * DAPI method Translations
 *
 * Gets translations for the given client_id and language. <br>
 *
 * http://dapi.diggerin.com/documentation/Translations.html
 *
 * No authorization required HTTP methods allowed: GET, POST
 *
 * Syntax: Translations/{client_id}/{language}/{keys}/
 *
 * Gets translations for the given client_id and language.
 *
 * The value for parameter client_id should correspond to one of the ids given by -Client/Versions- with version-number added (for instance like DiggerIn_Android_vTEST or DiggerIn_iOS_vTEST)
 *
 * If the language is not recognized then information about 'en-US' is returned (American English).
 * The parameter keys consists of a comma-separated list of keys that needs to be translated.
 *
 * Language can be specified in the same manner as the HTTP Header Field Accept-Language, like 'en-US', 'nb-NO', 'da-DK', 'sv-SE' (or just 'en', 'nb', 'da', 'sv').
 * You may store the preferred language as property "preferred_language" for the person.
 * (http://dapi.diggerin.com/documentation/PersonAddProperty.html with property_name "preferred_language")
 * Note that the returned 'version_id' may be stored locally and later checked with a quick call to http://dapi.diggerin.com/documentation/TranslationsVersion.html. (that is, to to avoid uneccessarily calling http://dapi.diggerin.com/documentation/Translations.html when there are no new data on the server.)\
 *
 * Note that this method call is available with HTTP POST in order to facilitate long lists of keys.
 *
 *
 */
+(NSMutableURLRequest*) getTranslationsClient_id: (NSString*)client_id language: (NSString*)language keys: (NSString*)keys;

+(NSMutableURLRequest*) postTranslationsClient_id: (NSString*)client_id language: (NSString*)language keys: (NSString*)keys;




/**
 * DAPI method Documentation/Refresh
 *
 * Refreshes the documentation.
 *
 * http://dapi.diggerin.com/documentation/DocumentationRefresh.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: Documentation/Refresh
 *
 * Refreshes the documentation.
 * Mostly useful for running repeated series of unit-tests where the id's used to update the database has to be unique.
 * For instance, the call http://dapi.diggerin.com/documentation/PersonAdd.html will fail if there already exists a person with email {email} in the database.
 * http://dapi.diggerin.com/documentation/DocumentationRefresh.html may in such cases be used to ensure a fresh set of unit-tests with a new unique value for {email}
 *
 *
 */
+(NSMutableURLRequest*) getDocumentationRefresh;




/**
 * DAPI method Component/Add
 *
 * Adds a Component to the database. <br>
 *
 * http://dapi.diggerin.com/documentation/ComponentAdd.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/Add/{component_type}/{registration_id}/
 * Syntax: Component/Add/{component_type}/
 *
 * Adds a Component to the database.
 *
 *
 * If {registration_id} is not given then the new component will automatically be assigned to the person identified by the authorization credentials used
 *
 * If {registration_id} is given it has to be unique. See also http://dapi.diggerin.com/documentation/ComponentAssign.html (used at system-level)
 *
 *
 */
+(NSMutableURLRequest*) getComponentAddComponent_type: (NSString*)component_type registration_id: (NSString*)registration_id;

+(NSMutableURLRequest*) postComponentAddComponent_type: (NSString*)component_type registration_id: (NSString*)registration_id;


/**
 * DAPI method Component/Add
 *
 * Adds a Component to the database. <br>
 *
 * http://dapi.diggerin.com/documentation/ComponentAdd.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/Add/{component_type}/{registration_id}/
 * Syntax: Component/Add/{component_type}/
 *
 * Adds a Component to the database.
 *
 *
 * If {registration_id} is not given then the new component will automatically be assigned to the person identified by the authorization credentials used
 *
 * If {registration_id} is given it has to be unique. See also http://dapi.diggerin.com/documentation/ComponentAssign.html (used at system-level)
 *
 *
 */
+(NSMutableURLRequest*) getComponentAddComponent_type: (NSString*)component_type;

+(NSMutableURLRequest*) postComponentAddComponent_type: (NSString*)component_type;




/**
 * DAPI method Component/At
 *
 * Stores script given by {script} for execution at time indicated by {time}
 *
 * http://dapi.diggerin.com/documentation/ComponentAt.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/At/{time}/{script}/
 *
 * Stores script given by {script} for execution at time indicated by {time}
 * Note that any colon in {time} is not allowed by our web-server (IIS) with syntax 1 even if you encode it as %3A. Encode as _COLON_ instead
 * Likewise for syntax 1, any forward slash in {script} must be replaced by _SLASH_ (encoding as %2F does not help)
 *
 * See also http://dapi.diggerin.com/documentation/PartAt.html
 */
+(NSMutableURLRequest*) getComponentAtTime: (NSString*)time script: (NSString*)script;

+(NSMutableURLRequest*) postComponentAtTime: (NSString*)time script: (NSString*)script;




/**
 * DAPI method Component/Property
 *
 * Returns property {name} for Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/{id}/Property/{name}/
 * Syntax: Component/{id}/Property/{name}/{field}/
 *
 * Returns property {name} for Component identified by {id}
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned). See http://dapi.diggerin.com/documentation/Component.html for more information about specifying {id}
 *
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed.
 *
 * {name} may consist of multiple property names separated by comma
 *
 * See also http://dapi.diggerin.com/documentation/PartProperty.html which shows more query options.
 */
+(NSMutableURLRequest*) getComponentPropertyId: (NSString*)id name: (NSString*)name;


/**
 * DAPI method Component/Property
 *
 * Returns property {name} for Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/{id}/Property/{name}/
 * Syntax: Component/{id}/Property/{name}/{field}/
 *
 * Returns property {name} for Component identified by {id}
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned). See http://dapi.diggerin.com/documentation/Component.html for more information about specifying {id}
 *
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed.
 *
 * {name} may consist of multiple property names separated by comma
 *
 * See also http://dapi.diggerin.com/documentation/PartProperty.html which shows more query options.
 */
+(NSMutableURLRequest*) getComponentPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;




/**
 * DAPI method Component/Represent
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id} where {id} refers to a component<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentRepresent.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/{id}/Represent/
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id} where {id} refers to a component
 *
 * This method is equivalent to http://dapi.diggerin.com/documentation/PersonRepresent.html with the only difference that {id} refers to a component instead of a person.
 * See http://dapi.diggerin.com/documentation/PersonRepresent.html for complete information.
 * {id} may be any of
 * component_id,
 * user_defined_name,
 * user_defined_group_name,
 *
 *
 * If {id} identifies more than one person an error response is generated
 * See also http://dapi.diggerin.com/documentation/PersonRepresent.html
 */
+(NSMutableURLRequest*) getComponentRepresentId: (NSString*)id;




/**
 * DAPI method Component/AddProperty
 *
 * Adds or changes a user-defined property for the Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentAddProperty.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/{id}/AddProperty/{property_name}/{value}/
 *
 * Adds or changes a user-defined property for the Component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will affect the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * For a list of properties which may be added or changed, use the HTML-version of http://dapi.diggerin.com/documentation/Component.html (add /HTML to the end of your URL).
 * All properties with a Save button next to them are user-changeable.
 */
+(NSMutableURLRequest*) getComponentAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;

+(NSMutableURLRequest*) postComponentAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;




/**
 * DAPI method Component/Run
 *
 * Runs all the Agent-Components identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentRun.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/{id}/Run/
 *
 * Runs all the Agent-Components identified by {id}
 *
 * Every agent's Run-method will update properties for the agent itself and possible also forpersons related to data related to the agent (for instance updating properties for performance of every seller associated with sales data related to the agent)
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * See http://dapi.diggerin.com/documentation/Component.html for more information about what kind of id's that can be used
 *
 *
 *
 */
+(NSMutableURLRequest*) getComponentRunId: (NSString*)id;




/**
 * DAPI method Component/Assign
 *
 * Assigns a Component to a person
 *
 * http://dapi.diggerin.com/documentation/ComponentAssign.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/{registration_id}/Assign/{person_id}/
 *
 * Assigns a Component to a person
 * This call is mostly used at system-level for setting up initial agents against non-personal entities like Salesforce
 * Assigns the Component designated by registration_id (case insensitive) to the person identified by person_id.The parameter person_id is optional. If not given the call will assign the component to the person identified by the authorization credentials used.
 *
 *
 * Note: As of January 2015 no unit-test is available for this call. Test manually instead by the examples provided.
 * See also http://dapi.diggerin.com/documentation/ComponentAdd.html, especially with 0 / null as registration id, this will also assign the component
 */
+(NSMutableURLRequest*) getComponentAssignRegistration_id: (NSString*)registration_id person_id: (NSString*)person_id;

+(NSMutableURLRequest*) postComponentAssignRegistration_id: (NSString*)registration_id person_id: (NSString*)person_id;




/**
 * DAPI method Component/History
 *
 * Returns history for the component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component//History
 * Syntax: Component/{id}/History/
 * Syntax: Component/{id}/History/{limit}/
 *
 * Returns history for the component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * component_id,
 * user_defined_name,
 * user_defined_group_name,
 *
 *
 * If {id} identifies more than one component an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getComponentHistory;


/**
 * DAPI method Component/History
 *
 * Returns history for the component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component//History
 * Syntax: Component/{id}/History/
 * Syntax: Component/{id}/History/{limit}/
 *
 * Returns history for the component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * component_id,
 * user_defined_name,
 * user_defined_group_name,
 *
 *
 * If {id} identifies more than one component an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getComponentHistoryId: (NSString*)id;


/**
 * DAPI method Component/History
 *
 * Returns history for the component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component//History
 * Syntax: Component/{id}/History/
 * Syntax: Component/{id}/History/{limit}/
 *
 * Returns history for the component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * component_id,
 * user_defined_name,
 * user_defined_group_name,
 *
 *
 * If {id} identifies more than one component an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getComponentHistoryId: (NSString*)id limit: (NSString*)limit;




/**
 * DAPI method Component/ListenerLog
 *
 * Returns DAPIListener log information about the Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentListenerLog.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/ListenerLog
 * Syntax: Component/{id}/ListenerLog/
 *
 * Returns DAPIListener log information about the Component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * Administrative access is required in order to access this method (note that if http://dapi.diggerin.com/documentation/PersonRepresent.html is used then access rights considered are that of the representative)
 * This method is meant for short-term using. Logging will be active for 10 minutes. After that the log will be deleted.
 * See http://dapi.diggerin.com/documentation/Component.html for more information about what kind of id's that can be used
 *
 *
 * If {id} identifies more than one component an error response is generated
 */
+(NSMutableURLRequest*) getComponentListenerLog;


/**
 * DAPI method Component/ListenerLog
 *
 * Returns DAPIListener log information about the Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentListenerLog.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/ListenerLog
 * Syntax: Component/{id}/ListenerLog/
 *
 * Returns DAPIListener log information about the Component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * Administrative access is required in order to access this method (note that if http://dapi.diggerin.com/documentation/PersonRepresent.html is used then access rights considered are that of the representative)
 * This method is meant for short-term using. Logging will be active for 10 minutes. After that the log will be deleted.
 * See http://dapi.diggerin.com/documentation/Component.html for more information about what kind of id's that can be used
 *
 *
 * If {id} identifies more than one component an error response is generated
 */
+(NSMutableURLRequest*) getComponentListenerLogId: (NSString*)id;




/**
 * DAPI method Component/Refresh
 *
 * Refreshes the component identified by {id} from original source<br>
 *
 * http://dapi.diggerin.com/documentation/ComponentRefresh.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component/SalesforceAgent/Refresh
 *
 * Refreshes the component identified by {id} from original source
 *
 * This method is relevant for components acting as container objects for parts coming from underlying systems. Like for instance a SalesforceAgent-component.
 *
 * {id} may be any of the following types:
 * component_id,
 * All (literally, like "Component/All"),
 * user_defined_name,
 * See http://dapi.diggerin.com/documentation/Component.html for more information about what kind of id's that can be used
 *
 *
 * See also http://dapi.diggerin.com/documentation/PartRefresh.html
 *
 */
+(NSMutableURLRequest*) getComponentSalesforceAgentRefresh;




/**
 * DAPI method Component
 *
 * Returns information about the Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Component.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component
 * Syntax: Component/{id}/
 *
 * Returns information about the Component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * {id} may be any of the following types:
 * component_id,
 * All (literally, like "Component/All"),
 * user_defined_name,
 * user_defined_group_name,
 * A quasi-SQL expression like "WHERE parameter_1 = 1" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 *
 *
 *
 */
+(NSMutableURLRequest*) getComponent;


/**
 * DAPI method Component
 *
 * Returns information about the Component identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Component.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Component
 * Syntax: Component/{id}/
 *
 * Returns information about the Component identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the component assigned to the person identified by the authorization credentials used. (in this case the call will fail if more than one component has been assigned)
 *
 * {id} may be any of the following types:
 * component_id,
 * All (literally, like "Component/All"),
 * user_defined_name,
 * user_defined_group_name,
 * A quasi-SQL expression like "WHERE parameter_1 = 1" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 *
 *
 *
 */
+(NSMutableURLRequest*) getComponentId: (NSString*)id;




/**
 * DAPI method Person/Logout
 *
 * Sets a flag resulting in the rejection of the HTTP Basic Authorization credentials next time they are supplied for authentication
 *
 * http://dapi.diggerin.com/documentation/PersonLogout.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/Logout
 *
 * Sets a flag resulting in the rejection of the HTTP Basic Authorization credentials next time they are supplied for authentication (in other words, the next HTTP request will fail with a 400 response code)
 * This call is useful when accessing the HTML user interface (like ui.diggerin.com) from a web-browserwith locally stored credentials when a change of user-identity is desired. It may then be easier to use this API-call than instruction the web-browser to clear the credentials.
 * Note that the flag will be reset the next time correct HTTP Basic Authorization credentials are supplied, regardless of from which client they occur. A new API-call (any call which requires authentication) should therefore be made immediately after this call in order to force the browser to ask for another set of credentials. If not, the flag may (theoretically) be reset by a call from another client, resulting in the browser not being hinted about asking for another set of credentials as desired.
 *
 *
 */
+(NSMutableURLRequest*) getPersonLogout;

+(NSMutableURLRequest*) postPersonLogout;




/**
 * DAPI method Person/Add
 *
 * Adds a new person to the database. The parameter {email} used has to be unique.<br>
 *
 * http://dapi.diggerin.com/documentation/PersonAdd.html
 *
 * No authorization required HTTP methods allowed: GET, POST
 *
 * Syntax: Person/Add/{email}/{password}/
 *
 * Adds a new person to the database. The parameter {email} used has to be unique.
 *
 * Returns the personId of the person just added.
 *
 * Note: It is also possible to add a Person by supplying only one parameter which will be regarded as 'organization_name'. This feature is useful for registering companies for which login-functionality is not needed.
 *
 * Possible data_error values for dapi_status are:"email_address_already_in_use_data_error"
 * "illegal_password_data_error"
 *
 *
 */
+(NSMutableURLRequest*) getPersonAddEmail: (NSString*)email password: (NSString*)password;

+(NSMutableURLRequest*) postPersonAddEmail: (NSString*)email password: (NSString*)password;




/**
 * DAPI method Person/AddAsChild
 *
 * Adds a new person to the database and creates a parent-child relationship with the person identified by the credentials used for authorization. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddAsChild.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddAsChild/{email}/{password}/
 * Syntax: Person/AddAsChild/{name}/
 *
 * Adds a new person to the database and creates a parent-child relationship with the person identified by the credentials used for authorization.
 *
 * The parameter {password} is optional. If the parameter {password} is not specified then the email-address is stored as "last_name" instead of as "email"
 *
 * This call is equivalent to first calling http://dapi.diggerin.com/documentation/PersonAdd.html and then http://dapi.diggerin.com/documentation/PersonAddChild.html.
 * It is meant for quick manual testing of the API and for quick manual setup of a new person hierarchy.
 * Possible data_error values for dapi_status in addition to that returned by http://dapi.diggerin.com/documentation/PersonAdd.html and http://dapi.diggerin.com/documentation/PersonAddChild.html are"different_parent_already_set_data_error"
 *
 *
 */
+(NSMutableURLRequest*) getPersonAddAsChildEmail: (NSString*)email password: (NSString*)password;

+(NSMutableURLRequest*) postPersonAddAsChildEmail: (NSString*)email password: (NSString*)password;


/**
 * DAPI method Person/AddAsChild
 *
 * Adds a new person to the database and creates a parent-child relationship with the person identified by the credentials used for authorization. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddAsChild.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddAsChild/{email}/{password}/
 * Syntax: Person/AddAsChild/{name}/
 *
 * Adds a new person to the database and creates a parent-child relationship with the person identified by the credentials used for authorization.
 *
 * The parameter {password} is optional. If the parameter {password} is not specified then the email-address is stored as "last_name" instead of as "email"
 *
 * This call is equivalent to first calling http://dapi.diggerin.com/documentation/PersonAdd.html and then http://dapi.diggerin.com/documentation/PersonAddChild.html.
 * It is meant for quick manual testing of the API and for quick manual setup of a new person hierarchy.
 * Possible data_error values for dapi_status in addition to that returned by http://dapi.diggerin.com/documentation/PersonAdd.html and http://dapi.diggerin.com/documentation/PersonAddChild.html are"different_parent_already_set_data_error"
 *
 *
 */
+(NSMutableURLRequest*) getPersonAddAsChildName: (NSString*)name;

+(NSMutableURLRequest*) postPersonAddAsChildName: (NSString*)name;




/**
 * DAPI method Person/Property
 *
 * Returns property {name} for Person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person/{id}/Property/{name}/
 * Syntax: Person/{id}/Property/{name}/{field}/
 *
 * Returns property {name} for Person identified by {id}
 * The parameter {id} is optional. If not given the call will return information about the person identified by the authorization credentials used.
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed.
 *
 * {name} may consist of multiple property names separated by comma
 *
 * See also http://dapi.diggerin.com/documentation/PartProperty.html which shows more query options.
 */
+(NSMutableURLRequest*) getPersonPropertyId: (NSString*)id name: (NSString*)name;


/**
 * DAPI method Person/Property
 *
 * Returns property {name} for Person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person/{id}/Property/{name}/
 * Syntax: Person/{id}/Property/{name}/{field}/
 *
 * Returns property {name} for Person identified by {id}
 * The parameter {id} is optional. If not given the call will return information about the person identified by the authorization credentials used.
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed.
 *
 * {name} may consist of multiple property names separated by comma
 *
 * See also http://dapi.diggerin.com/documentation/PartProperty.html which shows more query options.
 */
+(NSMutableURLRequest*) getPersonPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;




/**
 * DAPI method Person/Represent
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonRepresent.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person/{id}/Represent/
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id}
 *
 * This method is useful in a support-setting when the support representative needs to access data for a specific person over a set period of time.
 *
 * All further access towards DAPI with the same credentials used for authorization will behave as if it was done by the person represented.
 * The parameter {id} is optional. If not given any current representation for person identified by the credentials used will terminate.
 *
 * (It is also possible to terminate the representation by setting as no-longer-current the actual "person_to_represent"-property for the person identified by the credentials used)
 *
 * {id} must be a dapi_person_id
 *
 * Returns information about the person being represented (corresponding to method http://dapi.diggerin.com/documentation/Summary.html). The property "represented_by_person" will be set and
 * will contain the dapi_person_id of the person identified by the credentials used
 *
 *
 *
 */
+(NSMutableURLRequest*) getPersonRepresentId: (NSString*)id;




/**
 * DAPI method Person/AddProperty
 *
 * Adds a user-defined property for the persons identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddProperty.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddProperty/{property_name}/{value}/
 * Syntax: Person/{id}/AddProperty/{property_name}/{value}/
 *
 * Adds a user-defined property for the persons identified by {id}
 *
 * The {id} parameter is optional. If the {id} parameter is not specified it will be derived from the credentials used for authorization.
 *
 * {id} may be any of the following types:
 * person_id,
 * All (literally, like "Person//All/AddProperty"),
 *
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 * This method may also be used for changing the value of an existing property (By adding a new property with the same name the old property will be set as "not current")
 *
 *
 *   See also http://dapi.diggerin.com/documentation/NotificationAddEMail.html, http://dapi.diggerin.com/documentation/NotificationAddCellular.html and http://dapi.diggerin.com/documentation/NotificationAddApp.html.
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 *
 */
+(NSMutableURLRequest*) getPersonAddPropertyProperty_name: (NSString*)property_name value: (NSString*)value;

+(NSMutableURLRequest*) postPersonAddPropertyProperty_name: (NSString*)property_name value: (NSString*)value;


/**
 * DAPI method Person/AddProperty
 *
 * Adds a user-defined property for the persons identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddProperty.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddProperty/{property_name}/{value}/
 * Syntax: Person/{id}/AddProperty/{property_name}/{value}/
 *
 * Adds a user-defined property for the persons identified by {id}
 *
 * The {id} parameter is optional. If the {id} parameter is not specified it will be derived from the credentials used for authorization.
 *
 * {id} may be any of the following types:
 * person_id,
 * All (literally, like "Person//All/AddProperty"),
 *
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 * This method may also be used for changing the value of an existing property (By adding a new property with the same name the old property will be set as "not current")
 *
 *
 *   See also http://dapi.diggerin.com/documentation/NotificationAddEMail.html, http://dapi.diggerin.com/documentation/NotificationAddCellular.html and http://dapi.diggerin.com/documentation/NotificationAddApp.html.
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 *
 */
+(NSMutableURLRequest*) getPersonAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;

+(NSMutableURLRequest*) postPersonAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;




/**
 * DAPI method Person/AddChild
 *
 * Adds a new parent-child relationship between two persons to the database. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddChild/{child}/
 * Syntax: Person/{parent}/AddChild/{child}/
 *
 * Adds a new parent-child relationship between two persons to the database.
 * Parameter "child" refers to the subordinated person in the relationship while parameter "parent" referes to the super person / master person.
 *
 * Note that relationships may extend indefinitely in both directions with a child person being the parent person in another sub-ordinated relation and likewise, with a parent person being the child person in another super-ordinated relation.
 *
 * The {parent} parameter is optional. If the {parent} parameter is not specified it will be derived from the credentials used for authorization.
 *
 * The existence of relationships infer rights in the sense that the parent person may access all properties belonging to the child person.
 * The credentials used for authorization must contain the right to change properties of both the parent and child.
 * The easiest way to accomplish this is to just create the child with the parent's authorization. Alternatively, the method http://dapi.diggerin.com/documentation/PersonDelegateRight.html may be executed with the child's authorization before calling http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * See also http://dapi.diggerin.com/documentation/PersonDelegateRight.html
 *
 * Although the creation of a child-parent relationship is seen from the perspective of the parent API wise (syntax wise) the relationsship in itself will usually show up as a property of the child-object.
 *
 * See also http://dapi.diggerin.com/documentation/PersonAddAsChild.html which also creates the child-person as well as adding the parent-child relationship.
 *
 */
+(NSMutableURLRequest*) getPersonAddChildChild: (NSString*)child;

+(NSMutableURLRequest*) postPersonAddChildChild: (NSString*)child;


/**
 * DAPI method Person/AddChild
 *
 * Adds a new parent-child relationship between two persons to the database. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/AddChild/{child}/
 * Syntax: Person/{parent}/AddChild/{child}/
 *
 * Adds a new parent-child relationship between two persons to the database.
 * Parameter "child" refers to the subordinated person in the relationship while parameter "parent" referes to the super person / master person.
 *
 * Note that relationships may extend indefinitely in both directions with a child person being the parent person in another sub-ordinated relation and likewise, with a parent person being the child person in another super-ordinated relation.
 *
 * The {parent} parameter is optional. If the {parent} parameter is not specified it will be derived from the credentials used for authorization.
 *
 * The existence of relationships infer rights in the sense that the parent person may access all properties belonging to the child person.
 * The credentials used for authorization must contain the right to change properties of both the parent and child.
 * The easiest way to accomplish this is to just create the child with the parent's authorization. Alternatively, the method http://dapi.diggerin.com/documentation/PersonDelegateRight.html may be executed with the child's authorization before calling http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * See also http://dapi.diggerin.com/documentation/PersonDelegateRight.html
 *
 * Although the creation of a child-parent relationship is seen from the perspective of the parent API wise (syntax wise) the relationsship in itself will usually show up as a property of the child-object.
 *
 * See also http://dapi.diggerin.com/documentation/PersonAddAsChild.html which also creates the child-person as well as adding the parent-child relationship.
 *
 */
+(NSMutableURLRequest*) getPersonAddChildParent: (NSString*)parent child: (NSString*)child;

+(NSMutableURLRequest*) postPersonAddChildParent: (NSString*)parent child: (NSString*)child;




/**
 * DAPI method Person/DelegateRight
 *
 * Delegate rights from one person to another. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonDelegateRight.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/DelegateRight/{recipient}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/{delegator}/
 *
 * Delegate rights from one person to another.
 * Parameter {recipient} (delegate) refers to the person object being given delegated rights while parameter {delegator} refers to the person object whose rights are delegated.
 *
 * Note that delegation of rights may extend indefinitely in both directions with a delegator person being the recipient (delegate) person in another sub-ordinated delegation and likewise, with a recipient (delegate) person being the delegator person in another super-ordinated delegation.
 *
 * If only one parameter is specified it is taken as the recipient (delegate) parameter. Rights being delegated will then be "Change" and the delegator will be derived from the credentials used for authorization.
 *
 * The credentials used for authorization must contain the right to change properties of the delegator (no rights are needed for the recipient (delegate)).
 * The easiest way to accomplish this is to just authorize the method call with the delegators credentials and not supplying the delegator-parameter.
 *
 * See also http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * Note that syntax-wise multiple recipients and / or multiple delegators may be specified but this is not supported internally as of January 2015.
 *
 * The delegation of rights is seen from the perspective of the delegator API wise (syntax wise) but the delegated rights shows up as RelationDelegateDelegator-properties of both the delegator and recipient (delegate).
 * In addition the delegated rights will show up in the JSON-data as ReadRights and ChangeRights for the delegate and asReadDelegates and ChangeDelegates for the delegator.
 */
+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient;

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient;


/**
 * DAPI method Person/DelegateRight
 *
 * Delegate rights from one person to another. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonDelegateRight.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/DelegateRight/{recipient}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/{delegator}/
 *
 * Delegate rights from one person to another.
 * Parameter {recipient} (delegate) refers to the person object being given delegated rights while parameter {delegator} refers to the person object whose rights are delegated.
 *
 * Note that delegation of rights may extend indefinitely in both directions with a delegator person being the recipient (delegate) person in another sub-ordinated delegation and likewise, with a recipient (delegate) person being the delegator person in another super-ordinated delegation.
 *
 * If only one parameter is specified it is taken as the recipient (delegate) parameter. Rights being delegated will then be "Change" and the delegator will be derived from the credentials used for authorization.
 *
 * The credentials used for authorization must contain the right to change properties of the delegator (no rights are needed for the recipient (delegate)).
 * The easiest way to accomplish this is to just authorize the method call with the delegators credentials and not supplying the delegator-parameter.
 *
 * See also http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * Note that syntax-wise multiple recipients and / or multiple delegators may be specified but this is not supported internally as of January 2015.
 *
 * The delegation of rights is seen from the perspective of the delegator API wise (syntax wise) but the delegated rights shows up as RelationDelegateDelegator-properties of both the delegator and recipient (delegate).
 * In addition the delegated rights will show up in the JSON-data as ReadRights and ChangeRights for the delegate and asReadDelegates and ChangeDelegates for the delegator.
 */
+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right;

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right;


/**
 * DAPI method Person/DelegateRight
 *
 * Delegate rights from one person to another. <br>
 *
 * http://dapi.diggerin.com/documentation/PersonDelegateRight.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Person/DelegateRight/{recipient}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/
 * Syntax: Person/DelegateRight/{recipient}/{right}/{delegator}/
 *
 * Delegate rights from one person to another.
 * Parameter {recipient} (delegate) refers to the person object being given delegated rights while parameter {delegator} refers to the person object whose rights are delegated.
 *
 * Note that delegation of rights may extend indefinitely in both directions with a delegator person being the recipient (delegate) person in another sub-ordinated delegation and likewise, with a recipient (delegate) person being the delegator person in another super-ordinated delegation.
 *
 * If only one parameter is specified it is taken as the recipient (delegate) parameter. Rights being delegated will then be "Change" and the delegator will be derived from the credentials used for authorization.
 *
 * The credentials used for authorization must contain the right to change properties of the delegator (no rights are needed for the recipient (delegate)).
 * The easiest way to accomplish this is to just authorize the method call with the delegators credentials and not supplying the delegator-parameter.
 *
 * See also http://dapi.diggerin.com/documentation/PersonAddChild.html
 *
 * Note that syntax-wise multiple recipients and / or multiple delegators may be specified but this is not supported internally as of January 2015.
 *
 * The delegation of rights is seen from the perspective of the delegator API wise (syntax wise) but the delegated rights shows up as RelationDelegateDelegator-properties of both the delegator and recipient (delegate).
 * In addition the delegated rights will show up in the JSON-data as ReadRights and ChangeRights for the delegate and asReadDelegates and ChangeDelegates for the delegator.
 */
+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right delegator: (NSString*)delegator;

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right delegator: (NSString*)delegator;




/**
 * DAPI method Person/History
 *
 * Returns history for the person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person//History
 * Syntax: Person/{id}/History/
 * Syntax: Person/{id}/History/{limit}/
 *
 * Returns history for the person identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return history about the person identified by the authorization credentials used.
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * dapi_person_id,
 * email,
 * person_name,
 * person_group_name
 *
 *
 * If {id} identifies more than one person an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getPersonHistory;


/**
 * DAPI method Person/History
 *
 * Returns history for the person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person//History
 * Syntax: Person/{id}/History/
 * Syntax: Person/{id}/History/{limit}/
 *
 * Returns history for the person identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return history about the person identified by the authorization credentials used.
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * dapi_person_id,
 * email,
 * person_name,
 * person_group_name
 *
 *
 * If {id} identifies more than one person an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getPersonHistoryId: (NSString*)id;


/**
 * DAPI method Person/History
 *
 * Returns history for the person identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PersonHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person//History
 * Syntax: Person/{id}/History/
 * Syntax: Person/{id}/History/{limit}/
 *
 * Returns history for the person identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return history about the person identified by the authorization credentials used.
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of
 * dapi_person_id,
 * email,
 * person_name,
 * person_group_name
 *
 *
 * If {id} identifies more than one person an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getPersonHistoryId: (NSString*)id limit: (NSString*)limit;




/**
 * DAPI method Person
 *
 * Returns information about the person or persons identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Person.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person/{id}/
 * Syntax: Person/{id}/{extend_rights}/
 *
 * Returns information about the person or persons identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the person identified by the authorization credentials used.
 *
 * {id} may be any of
 * dapi_person_id,
 * All (literally, like "Person/All"),
 * email,
 * person_name,
 * person_group_name,
 * A quasi-SQL expression like "WHERE postal_code = '7014'" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * The optional parameter {extend_rights} may be set to 'True' if the concept of representation is used and all persons for which the representative has rights are desired.
 * This is applicable for the DAPI HTML admin-interface when searching for other persons to represent, leaving itunnecessary to "log out" from the represented person before looking up another person to represent
 * For more information about the concept of representation, see http://dapi.diggerin.com/documentation/PersonRepresent.html
 *
 * If {id} can potentially result in more than one person being returned, the return-data will always be in the form of an array of persons.
 *
 */
+(NSMutableURLRequest*) getPersonId: (NSString*)id;


/**
 * DAPI method Person
 *
 * Returns information about the person or persons identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Person.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Person/{id}/
 * Syntax: Person/{id}/{extend_rights}/
 *
 * Returns information about the person or persons identified by {id}
 *
 * The parameter {id} is optional. If not given the call will return information about the person identified by the authorization credentials used.
 *
 * {id} may be any of
 * dapi_person_id,
 * All (literally, like "Person/All"),
 * email,
 * person_name,
 * person_group_name,
 * A quasi-SQL expression like "WHERE postal_code = '7014'" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * The optional parameter {extend_rights} may be set to 'True' if the concept of representation is used and all persons for which the representative has rights are desired.
 * This is applicable for the DAPI HTML admin-interface when searching for other persons to represent, leaving itunnecessary to "log out" from the represented person before looking up another person to represent
 * For more information about the concept of representation, see http://dapi.diggerin.com/documentation/PersonRepresent.html
 *
 * If {id} can potentially result in more than one person being returned, the return-data will always be in the form of an array of persons.
 *
 */
+(NSMutableURLRequest*) getPersonId: (NSString*)id extend_rights: (NSString*)extend_rights;




/**
 * DAPI method Notification/AddEMail
 *
 * Adds an email notification recipient for the person<br>
 *
 * http://dapi.diggerin.com/documentation/NotificationAddEMail.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notification/AddEMail/{email}/
 * Syntax: Notification/AddEMail/{email}/{name}/
 *
 * Adds an email notification recipient for the person
 *
 * The method is equivalent to
 * Person/AddProperty/notification_recipient_email_X/{email}|{name}/
 * where X is the first "non-occupied" index found. (see http://dapi.diggerin.com/documentation/PersonAddProperty.html)
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotificationAddEMailEmail: (NSString*)email;

+(NSMutableURLRequest*) postNotificationAddEMailEmail: (NSString*)email;


/**
 * DAPI method Notification/AddEMail
 *
 * Adds an email notification recipient for the person<br>
 *
 * http://dapi.diggerin.com/documentation/NotificationAddEMail.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notification/AddEMail/{email}/
 * Syntax: Notification/AddEMail/{email}/{name}/
 *
 * Adds an email notification recipient for the person
 *
 * The method is equivalent to
 * Person/AddProperty/notification_recipient_email_X/{email}|{name}/
 * where X is the first "non-occupied" index found. (see http://dapi.diggerin.com/documentation/PersonAddProperty.html)
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotificationAddEMailEmail: (NSString*)email name: (NSString*)name;

+(NSMutableURLRequest*) postNotificationAddEMailEmail: (NSString*)email name: (NSString*)name;




/**
 * DAPI method Notification/AddCellular
 *
 * Adds a cellular notification recipient for the person<br>
 *
 * http://dapi.diggerin.com/documentation/NotificationAddCellular.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notification/AddCellular/{number}/
 * Syntax: Notification/AddCellular/{number}/{name}/
 *
 * Adds a cellular notification recipient for the person
 * By cellular is meant a telephone number capable of receiving voice-calls and SMS-messages.
 *
 * The method is equivalent to
 * Person/AddProperty/notification_recipient_cellular_X/{number}|{name}/
 * where X is the first "non-occupied" index found. (see http://dapi.diggerin.com/documentation/PersonAddProperty.html)
 *
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotificationAddCellularNumber: (NSString*)number;

+(NSMutableURLRequest*) postNotificationAddCellularNumber: (NSString*)number;


/**
 * DAPI method Notification/AddCellular
 *
 * Adds a cellular notification recipient for the person<br>
 *
 * http://dapi.diggerin.com/documentation/NotificationAddCellular.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notification/AddCellular/{number}/
 * Syntax: Notification/AddCellular/{number}/{name}/
 *
 * Adds a cellular notification recipient for the person
 * By cellular is meant a telephone number capable of receiving voice-calls and SMS-messages.
 *
 * The method is equivalent to
 * Person/AddProperty/notification_recipient_cellular_X/{number}|{name}/
 * where X is the first "non-occupied" index found. (see http://dapi.diggerin.com/documentation/PersonAddProperty.html)
 *
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotificationAddCellularNumber: (NSString*)number name: (NSString*)name;

+(NSMutableURLRequest*) postNotificationAddCellularNumber: (NSString*)number name: (NSString*)name;




/**
 * DAPI method Notification/AddApp
 *
 * Adds an identification for sending notifications to a Smartphone App<br>
 *
 * http://dapi.diggerin.com/documentation/NotificationAddApp.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notification/AddApp/{app_name}/{id}/
 *
 * Adds an identification for sending notifications to a Smartphone App
 *
 * Supports the following types of notifications:
 * 1) An Android GCM (Google Cloud Messaging) RegistrationId. See http://developer.android.com/google/gcm/index.html for more information. Or
 * 2) An APNS (Apple Push Notification Service) PartToken. See https://developer.apple.com/library/ios/documentation/NetworkingInternet/Conceptual/RemoteNotificationsPG/Chapters/ApplePushService.html for more information
 *
 * The value for parameter app_id should correspond to one of the ids (client_id's) given by -Client/Versions-
 *
 *
 * If the {app_name} and {id} (RegistrationId / PartToken) already exist then the actual property's last_valid field will be updated.
 * If the {app_name} and {id} do not exist then the method is equivalent to
 * Person/AddProperty/notification_recipient_app_X/{id}/
 * where X is the first "non-occupied" index found. (see http://dapi.diggerin.com/documentation/PersonAddProperty.html)
 *
 * Tip: Add a slash, /,  to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 * Note that a Smartphone App is expected to regularly confirm an id (RegistrationId / PartToken). Old ids may be purged regularly from DAPI. Ids may also be purged if they are confirmed invalid by the relevant service (Google Cloud Messaging / Apple Push Notification Service)
 */
+(NSMutableURLRequest*) getNotificationAddAppApp_name: (NSString*)app_name id: (NSString*)id;

+(NSMutableURLRequest*) postNotificationAddAppApp_name: (NSString*)app_name id: (NSString*)id;




/**
 * DAPI method Notify
 *
 * Sends notification<br>
 *
 * http://dapi.diggerin.com/documentation/Notify.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notify/{text}/
 * Syntax: Notify/{text}/{time_filter}/
 *
 * Sends notification
 * Sends notification with text {text} to all notification recipients registered with methods like
 * http://dapi.diggerin.com/documentation/NotificationAddEMail.html,
 *  http://dapi.diggerin.com/documentation/NotificationAddCellular.html,
 * http://dapi.diggerin.com/documentation/NotificationAddApp.html and
 *
 * If a notification with identical text has been sent after the point in time indicated by {time_filter} then the notification will be discarded. {time_filter} is optional, with default value of "10min".
 *
 *
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotifyText: (NSString*)text;

+(NSMutableURLRequest*) postNotifyText: (NSString*)text;


/**
 * DAPI method Notify
 *
 * Sends notification<br>
 *
 * http://dapi.diggerin.com/documentation/Notify.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Notify/{text}/
 * Syntax: Notify/{text}/{time_filter}/
 *
 * Sends notification
 * Sends notification with text {text} to all notification recipients registered with methods like
 * http://dapi.diggerin.com/documentation/NotificationAddEMail.html,
 *  http://dapi.diggerin.com/documentation/NotificationAddCellular.html,
 * http://dapi.diggerin.com/documentation/NotificationAddApp.html and
 *
 * If a notification with identical text has been sent after the point in time indicated by {time_filter} then the notification will be discarded. {time_filter} is optional, with default value of "10min".
 *
 *
 * Tip: Add / to the end of your query string if the last parameter contains a full stop . or dash - and you want to use HTTP method GET.
 */
+(NSMutableURLRequest*) getNotifyText: (NSString*)text time_filter: (NSString*)time_filter;

+(NSMutableURLRequest*) postNotifyText: (NSString*)text time_filter: (NSString*)time_filter;




/**
 * DAPI method Component/AddPart
 *
 * Adds a Part of {part_type} to the component identified by {id}
 *
 * http://dapi.diggerin.com/documentation/ComponentAddPart.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/{id}/AddPart/{part_type}/
 * Syntax: Component/{id}/AddPart/{part_type}/{parent_part_id}/
 *
 * Adds a Part of {part_type} to the component identified by {id}
 * Returns dapi_status OK if succeeds
 *
 *
 *
 */
+(NSMutableURLRequest*) getComponentAddPartId: (NSString*)id part_type: (NSString*)part_type;

+(NSMutableURLRequest*) postComponentAddPartId: (NSString*)id part_type: (NSString*)part_type;


/**
 * DAPI method Component/AddPart
 *
 * Adds a Part of {part_type} to the component identified by {id}
 *
 * http://dapi.diggerin.com/documentation/ComponentAddPart.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Component/{id}/AddPart/{part_type}/
 * Syntax: Component/{id}/AddPart/{part_type}/{parent_part_id}/
 *
 * Adds a Part of {part_type} to the component identified by {id}
 * Returns dapi_status OK if succeeds
 *
 *
 *
 */
+(NSMutableURLRequest*) getComponentAddPartId: (NSString*)id part_type: (NSString*)part_type parent_part_id: (NSString*)parent_part_id;

+(NSMutableURLRequest*) postComponentAddPartId: (NSString*)id part_type: (NSString*)part_type parent_part_id: (NSString*)parent_part_id;




/**
 * DAPI method Part/Aggregation/Current
 *
 * Same as -Part/Aggregation- but asks explicitly for the value for the current (and therefore still counting) {period}.
 *
 * http://dapi.diggerin.com/documentation/PartAggregationCurrent.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/Current/
 *
 * Same as http://dapi.diggerin.com/documentation/PartAggregation.html but asks explicitly for the value for the current (and therefore still counting) {period}.
 */
+(NSMutableURLRequest*) getPartAggregationCurrentId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;




/**
 * DAPI method Part/Aggregation/Last
 *
 * Same as -Part/Aggregation- but asks for the value for the last completed {period}.
 *
 * http://dapi.diggerin.com/documentation/PartAggregationLast.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/Last/
 *
 * Same as http://dapi.diggerin.com/documentation/PartAggregation.html but asks for the value for the last completed {period}.
 */
+(NSMutableURLRequest*) getPartAggregationLastId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;




/**
 * DAPI method Part/Aggregation/History
 *
 * Same as -Part/Aggregation- but returns multiple historical values instead of just the current value for the specified {period}.
 *
 * http://dapi.diggerin.com/documentation/PartAggregationHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/History/
 *
 * Same as http://dapi.diggerin.com/documentation/PartAggregation.html but returns multiple historical values instead of just the current value for the specified {period}.
 */
+(NSMutableURLRequest*) getPartAggregationHistoryId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;




/**
 * DAPI method Part/Aggregation
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.<br>
 *
 * http://dapi.diggerin.com/documentation/PartAggregation.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{period}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.
 * The parameters {aggregation_type} and {period} are optional, you may leave out either of them
 *
 * Use http://dapi.diggerin.com/documentation/PartAggregation.html to get a comprehensive list of possibilities (see all the links on the generated HTML-page)
 *
 * Typical values for {part_property} may be one of (value, state, current, total) depending on the actual part
 *
 * Possible values for {period} are one of (Min15, Hour, Day, Week, Month, Year, Ever)
 *
 * Typical values for {aggregation_type} may be one of (Sum, Min, Max, Average, Count_True, Count_False) depending on the actual part
 *
 * See also http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 */
+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property;


/**
 * DAPI method Part/Aggregation
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.<br>
 *
 * http://dapi.diggerin.com/documentation/PartAggregation.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{period}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.
 * The parameters {aggregation_type} and {period} are optional, you may leave out either of them
 *
 * Use http://dapi.diggerin.com/documentation/PartAggregation.html to get a comprehensive list of possibilities (see all the links on the generated HTML-page)
 *
 * Typical values for {part_property} may be one of (value, state, current, total) depending on the actual part
 *
 * Possible values for {period} are one of (Min15, Hour, Day, Week, Month, Year, Ever)
 *
 * Typical values for {aggregation_type} may be one of (Sum, Min, Max, Average, Count_True, Count_False) depending on the actual part
 *
 * See also http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 */
+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property period: (NSString*)period;


/**
 * DAPI method Part/Aggregation
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.<br>
 *
 * http://dapi.diggerin.com/documentation/PartAggregation.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Aggregation/{part_property}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{period}/
 * Syntax: Part/{id}/Aggregation/{part_property}/{aggregation_type}/{period}/
 *
 * Returns aggregation history for the part identified by {id} and the property identified by {part_property}.
 * The parameters {aggregation_type} and {period} are optional, you may leave out either of them
 *
 * Use http://dapi.diggerin.com/documentation/PartAggregation.html to get a comprehensive list of possibilities (see all the links on the generated HTML-page)
 *
 * Typical values for {part_property} may be one of (value, state, current, total) depending on the actual part
 *
 * Possible values for {period} are one of (Min15, Hour, Day, Week, Month, Year, Ever)
 *
 * Typical values for {aggregation_type} may be one of (Sum, Min, Max, Average, Count_True, Count_False) depending on the actual part
 *
 * See also http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 */
+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;




/**
 * DAPI method Aggregation
 *
 * Returns information about aggregation identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Aggregation.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Aggregation/{id}/
 *
 * Returns information about aggregation identified by {id}
 *
 * {id} may be a single aggregationId
 *
 *
 *
 */
+(NSMutableURLRequest*) getAggregationId: (NSString*)id;




/**
 * DAPI method Part/At
 *
 * Stores script given by {script} for execution at time indicated by {time}
 *
 * http://dapi.diggerin.com/documentation/PartAt.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Part/{id}/At/{time}/{script}/
 *
 * Stores script given by {script} for execution at time indicated by {time}
 * Note that any colon in {time} is not allowed by our web-server (IIS) with syntax 1 even if you encode it as %3A. Encode as _COLON_ instead
 * Likewise for syntax 1, any forward slash in {script} must be replaced by _SLASH_ (encoding as %2F does not help)
 *
 * See also http://dapi.diggerin.com/documentation/ComponentAt.html
 */
+(NSMutableURLRequest*) getPartAtId: (NSString*)id time: (NSString*)time script: (NSString*)script;

+(NSMutableURLRequest*) postPartAtId: (NSString*)id time: (NSString*)time script: (NSString*)script;




/**
 * DAPI method Part/Property
 *
 * Returns Property {name} for the part identified by {id}. <br>
 *
 * http://dapi.diggerin.com/documentation/PartProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Property/{name}/
 * Syntax: Part/{id}/Property/{name}/{field}/
 *
 * Returns Property {name} for the part identified by {id}.
 *
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed, and especially when a specific properties is needed for many different parts.
 *
 * {name} may consist of multiple property names separated by comma. {name} may end with a wildcard '%'. This is useful for properties which may be repeated, like notification_recipient_app_xx.
 *
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 * Note that even though it is possible in principle to combine an {id} resulting in more then one part, withmultiple property names separated by comma, it is not recommended as the ordering of the result is not guaranteed to be defined in any way.
 * Note that this method is somewhat "expensive" in the sense that whole parts are read from the database instead of just the properties requested.
 *
 * See also http://dapi.diggerin.com/documentation/ComponentProperty.html
 *
 */
+(NSMutableURLRequest*) getPartPropertyId: (NSString*)id name: (NSString*)name;


/**
 * DAPI method Part/Property
 *
 * Returns Property {name} for the part identified by {id}. <br>
 *
 * http://dapi.diggerin.com/documentation/PartProperty.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Property/{name}/
 * Syntax: Part/{id}/Property/{name}/{field}/
 *
 * Returns Property {name} for the part identified by {id}.
 *
 * Optionally a sub-field of property may be specified as the {field} parameter. This is useful if just some specific information is needed, and especially when a specific properties is needed for many different parts.
 *
 * {name} may consist of multiple property names separated by comma. {name} may end with a wildcard '%'. This is useful for properties which may be repeated, like notification_recipient_app_xx.
 *
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 * Note that even though it is possible in principle to combine an {id} resulting in more then one part, withmultiple property names separated by comma, it is not recommended as the ordering of the result is not guaranteed to be defined in any way.
 * Note that this method is somewhat "expensive" in the sense that whole parts are read from the database instead of just the properties requested.
 *
 * See also http://dapi.diggerin.com/documentation/ComponentProperty.html
 *
 */
+(NSMutableURLRequest*) getPartPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;




/**
 * DAPI method Part/Represent
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id} where {id} refers to a part<br>
 *
 * http://dapi.diggerin.com/documentation/PartRepresent.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Represent/
 *
 * Marks the person identified by the credentials used for authorization as representative of the person identified by {id} where {id} refers to a part
 *
 * This method is equivalent to http://dapi.diggerin.com/documentation/PersonRepresent.html with the only difference that {id} refers to a part instead of a person.
 * See http://dapi.diggerin.com/documentation/PersonRepresent.html for complete information.
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 *
 * If {id} identifies more than one person an error response is generated
 * See also http://dapi.diggerin.com/documentation/PersonRepresent.html
 */
+(NSMutableURLRequest*) getPartRepresentId: (NSString*)id;




/**
 * DAPI method Part/AddProperty
 *
 * Adds a user-defined property for the parts identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PartAddProperty.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Part/{id}/AddProperty/{property_name}/{value}/
 *
 * Adds a user-defined property for the parts identified by {id}
 * May also be used for changing the value of an existing property.
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 *
 */
+(NSMutableURLRequest*) getPartAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;

+(NSMutableURLRequest*) postPartAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;




/**
 * DAPI method Part/History
 *
 * Returns history for the part identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PartHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/History/
 * Syntax: Part/{id}/History/{limit}/
 *
 * Returns history for the part identified by {id}
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 *
 * If {id} identifies more than one part an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getPartHistoryId: (NSString*)id;


/**
 * DAPI method Part/History
 *
 * Returns history for the part identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PartHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/History/
 * Syntax: Part/{id}/History/{limit}/
 *
 * Returns history for the part identified by {id}
 *
 * The optional parameter {limit} may be used to narrow the result. See http://dapi.diggerin.com/documentation/PropertyHistory.html for explanation about using {limit}.
 *
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 *
 * If {id} identifies more than one part an error response is generated
 * See also -PropertyHistory-
 */
+(NSMutableURLRequest*) getPartHistoryId: (NSString*)id limit: (NSString*)limit;




/**
 * DAPI method Part/Refresh
 *
 * Refreshes the parts identified by {id} from original source<br>
 *
 * http://dapi.diggerin.com/documentation/PartRefresh.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/Refresh/
 *
 * Refreshes the parts identified by {id} from original source
 *
 * This method is relevant for Parts coming from underlying systems like Salesforce or similar.
 *
 * {id} may be any of user_defined_name, user_defined_group_name and dapi_part_id
 * See http://dapi.diggerin.com/documentation/Part.html for more information about what kind of id's that can be used
 *
 * See also http://dapi.diggerin.com/documentation/ComponentRefresh.html. Note that as of February 2015 http://dapi.diggerin.com/documentation/PartRefresh.html is equivalent to http://dapi.diggerin.com/documentation/ComponentRefresh.htmlfor the containing component.
 *
 *
 */
+(NSMutableURLRequest*) getPartRefreshId: (NSString*)id;




/**
 * DAPI method Part
 *
 * Returns information about the part or parts identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Part.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/
 * Syntax: Part/{id}/{component_id}/
 *
 * Returns information about the part or parts identified by {id}
 *
 * {id} may be any of the following types:
 * All (literally, like "Part/All"), (note that all parts for all components will be returned unless {component_id} is used to narrow down the search)
 * user_defined_name,
 * user_defined_group_name,
 * dapi_part_id
 * A quasi-SQL expression like "WHERE some_property = 'someValue'" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 * You may also add an optional parameter {component_id}, narrowing down the search to one or more specific component.
 * See http://dapi.diggerin.com/documentation/Component.html for how to specify components (component_id does not necessarily have to refer to only one component)
 *
 * Either a single Part is returned, or an array consisting of multiple parts.
 */
+(NSMutableURLRequest*) getPartId: (NSString*)id;


/**
 * DAPI method Part
 *
 * Returns information about the part or parts identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/Part.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Part/{id}/
 * Syntax: Part/{id}/{component_id}/
 *
 * Returns information about the part or parts identified by {id}
 *
 * {id} may be any of the following types:
 * All (literally, like "Part/All"), (note that all parts for all components will be returned unless {component_id} is used to narrow down the search)
 * user_defined_name,
 * user_defined_group_name,
 * dapi_part_id
 * A quasi-SQL expression like "WHERE some_property = 'someValue'" (this is experimental from January 2015 and is limited to examining a maximum of 100 persons, BEFORE access-rights are considered)
 * You may also search for multiple {id} values by separating each with comma. The different values may be of different types.
 *
 * You may also add an optional parameter {component_id}, narrowing down the search to one or more specific component.
 * See http://dapi.diggerin.com/documentation/Component.html for how to specify components (component_id does not necessarily have to refer to only one component)
 *
 * Either a single Part is returned, or an array consisting of multiple parts.
 */
+(NSMutableURLRequest*) getPartId: (NSString*)id component_id: (NSString*)component_id;




/**
 * DAPI method Property/History
 *
 * Shows historical values for property identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Property/{id}/History/
 * Syntax: Property/{id}/History/{limit}/{no_filter}/
 * Syntax: Property/{id}/History/{limit}/
 *
 * Shows historical values for property identified by {id}
 * {id} is usually found as the current property for a Part for instance.
 * The actual {id} does not necessarily have to be that of the last "current" property. The id of any old no-longer-current property may also be used.
 *
 * The optional parameter {limit} may be used to narrow the result. {limit} may be set to:
 * A) A maximum count of history records returned like '42' or
 * B) A relative time interval like '5min', '7hours' or similar (only newer records will be returned) or
 * C) An absolute point-in-time like '2015-01-13 10:00' (only newer records will be returned. Note that _COLON_ must be used instead of ':' if HTTP GET syntax 1 is used
 *
 * The optional parameter {no_filter} may be set to 'True' if absolutely all records are desired.
 * This is applicable for 'notification'-properties for which notifications deactivated by the person himself / herself are usually not shown.
 * In other words {no_filter} is useful when you want to see the person's notification-history, even messages that has been deleted from a corresponding app.
 *
 *
 * Note that if http://dapi.diggerin.com/documentation/PropertySetNoLongerCurrent.html has been called earlier with credentials corresponding to the PersonId1 of a property with Name 'notification', that property will not be included.
 *
 * See also http://dapi.diggerin.com/documentation/PartAggregation.html, http://dapi.diggerin.com/documentation/PersonHistory.html, http://dapi.diggerin.com/documentation/ComponentHistory.html and http://dapi.diggerin.com/documentation/PartHistory.html
 */
+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id;


/**
 * DAPI method Property/History
 *
 * Shows historical values for property identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Property/{id}/History/
 * Syntax: Property/{id}/History/{limit}/{no_filter}/
 * Syntax: Property/{id}/History/{limit}/
 *
 * Shows historical values for property identified by {id}
 * {id} is usually found as the current property for a Part for instance.
 * The actual {id} does not necessarily have to be that of the last "current" property. The id of any old no-longer-current property may also be used.
 *
 * The optional parameter {limit} may be used to narrow the result. {limit} may be set to:
 * A) A maximum count of history records returned like '42' or
 * B) A relative time interval like '5min', '7hours' or similar (only newer records will be returned) or
 * C) An absolute point-in-time like '2015-01-13 10:00' (only newer records will be returned. Note that _COLON_ must be used instead of ':' if HTTP GET syntax 1 is used
 *
 * The optional parameter {no_filter} may be set to 'True' if absolutely all records are desired.
 * This is applicable for 'notification'-properties for which notifications deactivated by the person himself / herself are usually not shown.
 * In other words {no_filter} is useful when you want to see the person's notification-history, even messages that has been deleted from a corresponding app.
 *
 *
 * Note that if http://dapi.diggerin.com/documentation/PropertySetNoLongerCurrent.html has been called earlier with credentials corresponding to the PersonId1 of a property with Name 'notification', that property will not be included.
 *
 * See also http://dapi.diggerin.com/documentation/PartAggregation.html, http://dapi.diggerin.com/documentation/PersonHistory.html, http://dapi.diggerin.com/documentation/ComponentHistory.html and http://dapi.diggerin.com/documentation/PartHistory.html
 */
+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id limit: (NSString*)limit no_filter: (NSString*)no_filter;


/**
 * DAPI method Property/History
 *
 * Shows historical values for property identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PropertyHistory.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Property/{id}/History/
 * Syntax: Property/{id}/History/{limit}/{no_filter}/
 * Syntax: Property/{id}/History/{limit}/
 *
 * Shows historical values for property identified by {id}
 * {id} is usually found as the current property for a Part for instance.
 * The actual {id} does not necessarily have to be that of the last "current" property. The id of any old no-longer-current property may also be used.
 *
 * The optional parameter {limit} may be used to narrow the result. {limit} may be set to:
 * A) A maximum count of history records returned like '42' or
 * B) A relative time interval like '5min', '7hours' or similar (only newer records will be returned) or
 * C) An absolute point-in-time like '2015-01-13 10:00' (only newer records will be returned. Note that _COLON_ must be used instead of ':' if HTTP GET syntax 1 is used
 *
 * The optional parameter {no_filter} may be set to 'True' if absolutely all records are desired.
 * This is applicable for 'notification'-properties for which notifications deactivated by the person himself / herself are usually not shown.
 * In other words {no_filter} is useful when you want to see the person's notification-history, even messages that has been deleted from a corresponding app.
 *
 *
 * Note that if http://dapi.diggerin.com/documentation/PropertySetNoLongerCurrent.html has been called earlier with credentials corresponding to the PersonId1 of a property with Name 'notification', that property will not be included.
 *
 * See also http://dapi.diggerin.com/documentation/PartAggregation.html, http://dapi.diggerin.com/documentation/PersonHistory.html, http://dapi.diggerin.com/documentation/ComponentHistory.html and http://dapi.diggerin.com/documentation/PartHistory.html
 */
+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id limit: (NSString*)limit;




/**
 * DAPI method Property/SetNoLongerCurrent
 *
 * Marks the property identified by {id} as 'not current'.<br>
 *
 * http://dapi.diggerin.com/documentation/PropertySetNoLongerCurrent.html
 *
 * HTTP methods allowed: GET, POST
 *
 * Syntax: Property/{id}/SetNoLongerCurrent/
 *
 * Marks the property identified by {id} as 'not current'.
 * The property is not deleted from the database, but 'not current' means that it will be ignored in normal circumstances.
 * You  may usually generate history for 'not current' properties.
 * Note that as of January 2015 it is allowed to mark any property as 'not current'. You may use http://dapi.diggerin.com/documentation/ComponentRefresh.html if you want to restore any accidentally marked properties.
 *
 */
+(NSMutableURLRequest*) getPropertySetNoLongerCurrentId: (NSString*)id;

+(NSMutableURLRequest*) postPropertySetNoLongerCurrentId: (NSString*)id;




/**
 * DAPI method Property/Run
 *
 * Runs the script given by the property identified by {id}<br>
 *
 * http://dapi.diggerin.com/documentation/PropertyRun.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Property/{id}/Run/
 *
 * Runs the script given by the property identified by {id}
 *
 *
 *
 */
+(NSMutableURLRequest*) getPropertyRunId: (NSString*)id;




/**
 * DAPI method Property
 *
 * Returns information about the property identified by {id}
 *
 * http://dapi.diggerin.com/documentation/Property.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: Property/{id}/
 *
 * Returns information about the property identified by {id}
 */
+(NSMutableURLRequest*) getPropertyId: (NSString*)id;




/**
 * DAPI method HTTPStatus
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}<br>
 *
 * http://dapi.diggerin.com/documentation/HTTPStatus.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: HTTPStatus
 * Syntax: HTTPStatus/{status_code}/
 * Syntax: HTTPStatus/{status_code}/{message}/
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}
 *
 * Not used by the API itself. Used for presentation of static HTML pages in order to have standardized error-pages
 *
 * If {status_code} is not given or if {status_code} is an invalid HTTP status code then 404 is assumed.
 * If {message} is not given then "Page not found" is assumed.
 * if {status_code} starts with '3' and {message} starts with 'http' then the HTTP Location header in the returned response will be set to {message}
 * HTML is returned regardless of HTML being added at the end of the query-string or not.
 *
 *
 */
+(NSMutableURLRequest*) getHTTPStatus;


/**
 * DAPI method HTTPStatus
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}<br>
 *
 * http://dapi.diggerin.com/documentation/HTTPStatus.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: HTTPStatus
 * Syntax: HTTPStatus/{status_code}/
 * Syntax: HTTPStatus/{status_code}/{message}/
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}
 *
 * Not used by the API itself. Used for presentation of static HTML pages in order to have standardized error-pages
 *
 * If {status_code} is not given or if {status_code} is an invalid HTTP status code then 404 is assumed.
 * If {message} is not given then "Page not found" is assumed.
 * if {status_code} starts with '3' and {message} starts with 'http' then the HTTP Location header in the returned response will be set to {message}
 * HTML is returned regardless of HTML being added at the end of the query-string or not.
 * 
 * 
 */
+(NSMutableURLRequest*) getHTTPStatusStatus_code: (NSString*)status_code;


/**
 * DAPI method HTTPStatus
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}<br>
 *
 * http://dapi.diggerin.com/documentation/HTTPStatus.html
 *
 * No authorization required HTTP methods allowed: GET
 *
 * Syntax: HTTPStatus
 * Syntax: HTTPStatus/{status_code}/
 * Syntax: HTTPStatus/{status_code}/{message}/
 *
 * Returns an HTTP / HTML-respons with the specified HTTP {status_code} and the specified plain-text {message}
 * 
 * Not used by the API itself. Used for presentation of static HTML pages in order to have standardized error-pages
 * 
 * If {status_code} is not given or if {status_code} is an invalid HTTP status code then 404 is assumed.
 * If {message} is not given then "Page not found" is assumed.
 * if {status_code} starts with '3' and {message} starts with 'http' then the HTTP Location header in the returned response will be set to {message}
 * HTML is returned regardless of HTML being added at the end of the query-string or not.
 * 
 * 
 */
+(NSMutableURLRequest*) getHTTPStatusStatus_code: (NSString*)status_code message: (NSString*)message;




/**
 * DAPI method ExceptionDetails
 *
 * Gets details for the last Exception that occurred on the server.<br>
 *
 * http://dapi.diggerin.com/documentation/ExceptionDetails.html
 *
 * HTTP methods allowed: GET
 *
 * Syntax: ExceptionDetails/{sub_system}/
 *
 * Gets details for the last Exception that occurred on the server.
 * This method requires administrative credentials
 * 
 * 
 * 
 */
+(NSMutableURLRequest*) getExceptionDetailsSub_system: (NSString*)sub_system;



@end