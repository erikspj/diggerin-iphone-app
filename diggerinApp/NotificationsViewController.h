//
//  NotificationsViewController.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 02.12.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#ifndef diggerinApp_NotificationsViewController_h
#define diggerinApp_NotificationsViewController_h

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "constants.h"

@interface NotificationsViewController : UITableViewController <UITableViewDelegate, UITableViewDataSource, NSURLConnectionDelegate>

@end

#endif
