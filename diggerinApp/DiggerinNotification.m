//
//  DiggerinNotification.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 01.12.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import "DiggerinNotification.h"


@implementation DiggerinNotification

#pragma mark init

- (id)initWithValuesDate:(NSDate *)date
                  isRead:(Boolean)isRead
                 subject:(NSString *)subject
                 emailId:(long)emailId
                    mrId:(long)mrId
                viewName:(NSString *)vName
                   algId:(long)algId
                  alName:(NSString *)alName
                    type:(NSString *)type
{
    self = [super init];
    
    if (self)
    {
        _date = date;
        _isRead = isRead;
        _subject = subject;
        _emailId = emailId;
        _mrId = mrId;
        _vName = vName;
        _algId = algId;
        _alName = alName;
        _type = type;
    }
    
    return self;
}

- (id)initWithDictionary:(NSDictionary *)userInfo
{
    return [self initWithValuesLongLongDate:[userInfo[kDateKey] longLongValue]
                                     isRead:[userInfo[kIsReadKey] boolValue]
                                    subject:userInfo[kSubjectKey]
                                    emailId:[userInfo[kEmailIdKey] integerValue]
                                       mrId:[userInfo[kMrIdKey] integerValue]
                                   viewName:userInfo[kViewNameKey]
                                      algId:[userInfo[kAlgIdKey] integerValue]
                                     alName:userInfo[kAlNameKey]
                                       type:userInfo[kTypeKey]];
}

- (id)initWithValuesLongLongDate:(long long)date
                          isRead:(Boolean)isRead
                         subject:(NSString *)subject
                         emailId:(long)emailId
                            mrId:(long)mrId
                        viewName:(NSString *)vName
                           algId:(long)algId
                          alName:(NSString *)alName
                            type:(NSString *)type
{
    return [self initWithValuesDate:[[NSDate alloc] initWithTimeIntervalSince1970:date / 1000]
                             isRead:isRead
                            subject:subject
                            emailId:emailId
                               mrId:mrId
                           viewName:vName
                              algId:algId
                             alName:alName
                               type:type];
}

- (id)initWithExampleValues
{
    NSDateFormatter *yyyyMMddhhmmss = [[NSDateFormatter alloc] init];
    [yyyyMMddhhmmss setTimeStyle:NSDateFormatterNoStyle];
    [yyyyMMddhhmmss setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    return [self initWithValuesDate:[yyyyMMddhhmmss dateFromString:NSLocalizedStringFromTable(kDateKey, kTestNotificationTable, nil)]
                             isRead:[NSLocalizedStringFromTable(kIsReadKey, kTestNotificationTable, nil) boolValue]
                            subject:NSLocalizedStringFromTable(kSubjectKey, kTestNotificationTable, nil)
                            emailId:[NSLocalizedStringFromTable(kEmailIdKey, kTestNotificationTable, nil) intValue]
                               mrId:[NSLocalizedStringFromTable(kMrIdKey, kTestNotificationTable, nil) intValue]
                           viewName:NSLocalizedStringFromTable(kViewNameKey, kTestNotificationTable, nil)
                              algId:[NSLocalizedStringFromTable(kAlgIdKey, kTestNotificationTable, nil) intValue]
                             alName:NSLocalizedStringFromTable(kAlNameKey, kTestNotificationTable, nil)
                               type:NSLocalizedStringFromTable(kTypeKey, kTestNotificationTable, nil)];
}

#pragma mark Getter Methods

- (NSString *)dateAsString
{
    return [NSDateFormatter localizedStringFromDate:self.date dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterFullStyle];
}

- (NSString *)stringified
{
    return [[NSString stringWithFormat:@"{%@=%ld,%@=%@,%@=%@,%@=%lld,%@=%@,%@=%ld,%@=%d,%@=%@,%@=%ld}",
             kEmailIdKey, self.emailId,
             kTypeKey, self.type,
             kSubjectKey, self.subject,
             kDateKey, (long long)self.date.timeIntervalSince1970 * 1000,
             kViewNameKey, self.vName,
             kAlgIdKey, self.algId,
             kIsReadKey, self.isRead,
             kAlNameKey, self.alName,
             kMrIdKey, self.mrId]
            stringByReplacingOccurrencesOfString:kSpace withString:kSubstitute];
}
@end