//
//  NotificationsViewController.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 02.12.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import "NotificationsViewController.h"

@interface NotificationsViewController ()

@property (weak, nonatomic) IBOutlet UITableView *notificationsTable;
@property (weak, nonatomic) NSMutableArray *notifications;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation NotificationsViewController



#pragma mark View Methods

// Called when the view appears
// Reloads the table of notifications
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!self.notifications)
    {
        self.notifications = ((AppDelegate *)[UIApplication sharedApplication].delegate).notifications;
    }
    
    [self.notificationsTable reloadData];
    
    
}

#pragma mark UITableViewDataSource Methods

// Called when the number of rows in the tableView is requested.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.notifications.count;
}

// Called when the cell at indexPath is populated
// Sets the cell's image as a "trigger" icon.
// Adds a tapped GestureRecognizer that calls the triggerTapped: method to the cell's image
// Sets the cell's image's tag as the indexPath.row
// Sets the cell's text as the subject of the DiggerinNotification at indexPath.row in the NSMutableArray notifications.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DiggerinNotification *notification = self.notifications[indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kTSimpleTableItemId];

    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kTSimpleTableItemId];
    }
    
    UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(triggerTapped:)];
    tapped.numberOfTapsRequired = 1;
    [cell.imageView addGestureRecognizer:tapped];
    cell.imageView.image = [UIImage imageNamed:kTriggerImageKey];
    cell.imageView.userInteractionEnabled = YES;
    cell.imageView.tag = indexPath.row;
    
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.numberOfLines = 0;
//    cell.textLabel.backgroundColor = [UIColor viewFlipsideBackgroundColor];
//    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.text = notification.subject;
    
    return cell;
}

#pragma mark UITableViewDelegate Methods

// Called when a row in the UITableView is selected.
// Removes the DiggerinNotification at indexPath.row from the NSMutableArray notifications.
// Updates the badge of the application's icon to the new number of DiggerinNotifications.
// Opens a web page showing the DiggerinNotification in the phone's browser.
// Closes the view.
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    DiggerinNotification *notification = self.notifications[indexPath.row];
    NSURL *uri = [[NSURL alloc] initWithString:[NSString stringWithFormat:NSLocalizedStringFromTable(kEmailUriKey, kSharedTable, nil),
                                                (long)notification.algId,
                                                (long)notification.emailId,
                                                kUserId,
                                                kCompanyId]];
    if (self.notifications.count == 1)
    {
        [appDelegate willChangeValueForKey:kNotifications];
    }
    [self.notifications removeObjectAtIndex:indexPath.row];
    if (self.notifications.count == 0)
    {
        [appDelegate didChangeValueForKey:kNotifications];
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = self.notifications.count;
    
    [self markAsRead:notification.emailId];
    
    [[UIApplication sharedApplication] openURL:uri];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark IBAction Methods

// Called when the ok button is tapped
// Closes the view.
- (IBAction)okTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Called when the trigger icon is tapped
// Opens a web page showing the DiggerinNotification's triggering rule in the phone's browser.
- (void)triggerTapped:(id)sender
{
    UITapGestureRecognizer *tap = (UITapGestureRecognizer *)sender;
    DiggerinNotification *notification = self.notifications[tap.view.tag];
    NSURL *uri = [[NSURL alloc] initWithString:[NSString stringWithFormat:NSLocalizedStringFromTable(kTriggerUriKey, kSharedTable, nil),
                                                notification.algId,
                                                notification.alName,
                                                notification.vName]];
    
    [[UIApplication sharedApplication] openURL:uri];
}

#pragma mark Helper Methods

- (void)markAsRead:(long)notificationId
{
    NSURLRequest *request = nil; //[DAPI getMarkAsRead:notificationId];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (!connection)
    {
        [ViewController showAlertWithTitle:NSLocalizedString(kMarkAsReadErrorKey, nil)
                                andMessage:[NSString stringWithFormat:NSLocalizedString(kMarkAsReadErrorWithParameterKey, nil), kNetworkErrorKey]
                          onViewController:self];
    }
}

#pragma mark NSURLConnection Methods

// Called when there is some sort of response from the DAPI
// Initializes responseData
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
}

// Called when the object receives data
// Appends the received data to responseData
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

// Do not cache response
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

// Called when the response stream has finished
// Calls the ViewController to show an information message if the marking succeeded,
// or to show an error message if not.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&error];
    
    if ([result[kDataKey][kScalarResultKey] boolValue])
    {
        [ViewController showAlertWithTitle:NSLocalizedString(kMarkedAsReadTitleKey, nil)
                                andMessage:NSLocalizedString(kMarkedAsReadKey, nil)
                          onViewController:self];
    }
    else
    {
        [ViewController showAlertWithTitle:NSLocalizedString(kMarkAsReadErrorKey, nil)
                                andMessage:NSLocalizedString(kMarkAsReadErrorKey, nil)
                          onViewController:self];
    }
}

// Called if the connection failed
// Calls the ViewController to show the error message
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [ViewController showAlertWithTitle:NSLocalizedString(kMarkAsReadErrorKey, nil)
                            andMessage:[NSString stringWithFormat:NSLocalizedString(kMarkAsReadErrorWithParameterKey, nil),
                                        error.userInfo[NSLocalizedDescriptionKey]]
                      onViewController:self];
}

@end