//
//  ViewController.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 03.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController()

@property (weak, nonatomic) IBOutlet UIButton *notificationsButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) NSMutableData *responseData;

@end

@implementation ViewController

#pragma mark Class Methods

// Called by the other ViewControllers to show an alert with a message and an OK button.
+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message onViewController:(UIViewController *)viewController
{
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:kOk style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {}];
    [alert addAction:defaultAction];
    
    [viewController presentViewController:alert animated:YES completion:nil];
}

// Called by the AppDelegate to show an alert with a message and an OK button.
- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message
{
    [self.class showAlertWithTitle:title andMessage:message onViewController:self];
}

#pragma mark View Methods

// Called when the view has finished loading.
// Hides the view in case the activation view should be called.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.hidden = YES;
}

// Called when the view appears
// Shows the test button if there exist received remote notifications.
// If the Diggerin notification system is activated, show the view;
// if not, start the ActivationViewController.
// Hide the test button if we are not registered for remote notifications.
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    self.appDelegate = (AppDelegate *)application.delegate;
    
    [self.appDelegate addObserver:self forKeyPath:kNotifications options:0 context:nil];
    self.notificationsButton.hidden = (!self.appDelegate.hasNotifications);
    
    if (self.appDelegate.loggedIn)
    {
        self.view.hidden = NO;
    }
    else
    {
        [self performSegueWithIdentifier:kActivationKey sender:self];
    }
    
//    test.hidden = !application.isRegisteredForRemoteNotifications;
}

#pragma mark Segue Methods

// Called before another ViewController is called.
// If the called ViewController is the ActivationViewController, set this ViewController as the called ViewController's delegate.
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:kActivationKey])
    {
        ((ActivationViewController *)segue.destinationViewController).delegate = self;
    }
}

#pragma mark Key Value Object Methods

// Called when the observed value changes.
// Hides the test button if there are no received notifications left in the AppDelegate's NSArray notifications.
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSArray *notifications = ((AppDelegate *)object).notifications;
    
    self.notificationsButton.hidden = (notifications.count == 0);
}

#pragma mark ActivationViewController Delegate Methods

// Called by the ActivationViewController.
// Sets the appDelegate's credential.
// Closes the ActivationViewController.
// Registers the application for displaying alerts when receiving remote notifications and for badges (numbers) on the application's icon.
// Registers the application for remote notifications.
- (void)activateWithCredential:(NSURLCredential *)credential stayLoggedIn:(Boolean)stayLoggedIn
{
    [self.appDelegate activateWithCredential:credential stayLoggedIn:stayLoggedIn];
    [self dismissViewControllerAnimated:YES completion:nil];
    
    UIApplication *application = [UIApplication sharedApplication];

    [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                   settingsForTypes:(UIUserNotificationTypeAlert|UIUserNotificationTypeBadge)
                                                   categories:nil]];
    [application registerForRemoteNotifications];
    
}

#pragma mark IBAction Methods

// Called when the test button is tapped.
// Starts the activityIndicator.
// Calls the DAPI to send a test notification.
- (IBAction)testTapped:(id)sender {
    [self.activityIndicator startAnimating];

    DiggerinNotification *notification = [[DiggerinNotification alloc] initWithExampleValues];
    NSURLRequest *request = [DAPI getNotifyText:notification.stringified];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (!connection)
    {
        [self showAlertWithTitle:NSLocalizedString(kNotificationFailedKey, nil)
                      andMessage:[NSString stringWithFormat:NSLocalizedString(kNotificationFailedWithParameterKey, nil),
                                  NSLocalizedString(kNetworkErrorKey, nil)]];
    }
}

#pragma mark NSURLConnection Delegate Methods

// Called when there is some sort of response from the DAPI
// Initializes responseData
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
}

// Called when the object receives data
// Appends the received data to responseData
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

// Do not cache response
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

// Called when the response stream has finished
// Stops the activity indicator.
// Calls the ViewController to show an error message if the message was discarded.
// Calls the ViewController to show an information message if the message was successfully processed.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    NSDictionary *dapi_result = [NSJSONSerialization JSONObjectWithData:self.responseData
                                                                options:NSJSONReadingMutableLeaves
                                                                  error:&error][kDataKey][kDapiResultKey];
    
    [self.activityIndicator stopAnimating];
    
    if (dapi_result[kNotificationDiscardedKey])
    {
        [self showAlertWithTitle:NSLocalizedString(kMessageDiscardedKey, nil)
                      andMessage:NSLocalizedString(kIdenticalMessageKey, nil)];
    }
    else
    {
        int numberOfQueuedMessages = [dapi_result[kQueueKey] intValue];
        int numberOfExceptions = [dapi_result[kExceptionKey] intValue];
        NSString *message = [NSString stringWithFormat:@"%@ %@",
                             [NSString localizedStringWithFormat:NSLocalizedString(kMessagesQueuedWithParameterKey, nil), numberOfQueuedMessages],
                             [NSString localizedStringWithFormat:NSLocalizedString(kExceptionsWithParameterKey, nil), numberOfExceptions]];
        
        [self showAlertWithTitle:NSLocalizedString(kMessageProcessedKey, nil)
                      andMessage:message];
    }
}

// Called if the connection failed
// Calls the ViewController to show the user the error message
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    
    [self showAlertWithTitle:NSLocalizedString(kNotificationFailedKey, nil)
                  andMessage:[NSString stringWithFormat:NSLocalizedString(kNotificationFailedWithParameterKey, nil),
                              error.userInfo[NSLocalizedDescriptionKey]]];
}

// Called if the DAPI requests an authentication
// Sends the appDelegate's credential member to the sender if the authentication succeedes.
// Calls the ViewController to show an error message if the authentication fails.
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if (challenge.previousFailureCount == 0)
    {
        [challenge.sender useCredential:self.appDelegate.credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self showAlertWithTitle:NSLocalizedString(kAuthenticationFailedKey, nil)
                      andMessage:NSLocalizedString(kWrongUsernameOrPasswordKey, nil)];
    }
}
@end
