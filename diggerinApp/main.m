//
//  main.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 03.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
