//
//  LoginViewController.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 29.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import "ActivationViewController.h"

@interface ActivationViewController ()

@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) NSURLCredential *credential;
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UISwitch *stayLoggedIn;

@end

@implementation ActivationViewController

#pragma mark View Methods

// Set this object as delegate for events in the UITextFields username and password
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.username.delegate = self;
    self.password.delegate = self;
}

#pragma mark IBAction Methods

// Called when the Activate button is tapped.
// Start the activityIndicator
// Call DAPI VerifyCredentials
// Show error message if the request fails
- (IBAction)activateTapped:(id)sender
{
    [self.activityIndicator startAnimating];
    
    NSURLRequest *request = [DAPI getVerifyCredentials];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (!connection)
    {
        [self.activityIndicator stopAnimating];

        [self.delegate.class showAlertWithTitle:NSLocalizedString(kActivationErrorKey, nil)
                                     andMessage:[NSString stringWithFormat:NSLocalizedString(kActivationErrorWithParameterKey, nil), NSLocalizedString(kNetworkErrorKey, nil)]
                               onViewController:self];
    }
}

#pragma mark UITextField Delegate Methods

// Called when the Enter button is tapped in the UITextField username or password
// Sets focus to the next UITextField if the calling UITextField is username
// Calls the activateTapped: method if the calling UITextField is password
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.username)
    {
        [self.password becomeFirstResponder];
        
        return NO;
    }
    else if (textField == self.password)
    {
        [self activateTapped:self];
        
        return NO;
    }
    
    return YES;
}

#pragma mark NSURLConnection Delegate Methods

// Called when there is some sort of response from the DAPI
// Initializes responseData
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
}

// Called when the object receives data
// Appends the received data to responseData
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

// Do not cache response
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

// Called when the response stream has finished
// Stops the activityIndicator
// Calls the delegate's (ViewController's) setCredential method if the scalar_result field of the responseData is True,
// shows an error message if not
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&error];
    
    [self.activityIndicator stopAnimating];
    
    if ([result[kDataKey][kScalarResultKey] boolValue])
    {
        [self.delegate activateWithCredential:self.credential stayLoggedIn:self.stayLoggedIn.on];
    }
    else
    {
        [self.delegate.class showAlertWithTitle:NSLocalizedString(kActivationFailedKey, nil)
                                     andMessage:NSLocalizedString(kWrongUsernameOrPasswordKey, nil)
                               onViewController:self];
    }
}

// Called if the connection failed
// Calls the ViewController to show the error message
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.activityIndicator stopAnimating];
    
    [self.delegate.class showAlertWithTitle:NSLocalizedString(kActivationErrorKey, nil)
                                 andMessage:[NSString stringWithFormat:NSLocalizedString(kActivationErrorWithParameterKey, nil),
                                             error.userInfo[NSLocalizedDescriptionKey]]
                           onViewController:self];
    
}

// Called if the DAPI requests an authentication
// Creates the credential member from the text values of the UITextFields username and password if the authentication did not fail,
// stops the activityIndicator and calls the ViewController to show an error message if the authentication did fail
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if (challenge.previousFailureCount == 0)
    {
        self.credential = [NSURLCredential credentialWithUser:self.username.text
                                                     password:self.password.text
                                                  persistence:NSURLCredentialPersistenceForSession];
        [challenge.sender useCredential:self.credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [self.activityIndicator stopAnimating];
        
        [self.delegate.class showAlertWithTitle:NSLocalizedString(kAuthenticationFailedKey, nil)
                                     andMessage:NSLocalizedString(kWrongUsernameOrPasswordKey, nil)
                               onViewController:self];
    }
}
@end
