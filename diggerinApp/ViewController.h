//
//  ViewController.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 03.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#ifndef diggerinApp_ViewController_h
#define diggerinApp_ViewController_h

#import <UIKit/UIKit.h>
#import "ActivationViewController.h"
#import "AppDelegate.h"
#import "constants.h"

@interface ViewController : UIViewController<ActivationDelegate, NSURLConnectionDelegate>

+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message onViewController:(UIViewController *)viewController;
- (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message;

@end

#endif