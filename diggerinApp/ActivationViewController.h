//
//  LoginViewController.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 29.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#ifndef diggerinApp_LoginViewController_h
#define diggerinApp_LoginViewController_h

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "DAPI.h"
#import "constants.h"

@protocol ActivationDelegate <NSObject>

+ (void)showAlertWithTitle:(NSString *)title andMessage:(NSString *)message onViewController:(UIViewController *)viewController;
- (void)activateWithCredential:(NSURLCredential *)credential stayLoggedIn:(Boolean)stayLoggedIn;

@end

@interface ActivationViewController: UIViewController <UITextFieldDelegate, NSURLConnectionDelegate>

@property (weak, nonatomic)id <ActivationDelegate> delegate;

@end

#endif