//
//  AppDelegate.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 03.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#ifndef diggerinApp_AppDelegate_h
#define diggerinApp_AppDelegate_h

#import <UIKit/UIKit.h>
#import "DiggerinNotification.h"
#import "NotificationsViewController.h"
#import "ViewController.h"
#import "DAPI.h"
#import "KeychainItemWrapper.h"
#import "constants.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSURLCredential *credential;
@property (strong, nonatomic) NSMutableArray *notifications;
@property (nonatomic, readonly, getter=isLoggedIn) Boolean loggedIn;
@property (nonatomic, readonly) Boolean hasNotifications;

- (void)activateWithCredential:(NSURLCredential *)credential stayLoggedIn:(Boolean)stayLoggedIn;

@end

#endif
