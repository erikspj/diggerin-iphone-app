//
//  DiggerinNotification.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 01.12.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#ifndef diggerinApp_DiggerinNotification_h
#define diggerinApp_DiggerinNotification_h

#import <Foundation/Foundation.h>
#import "constants.h"

@interface DiggerinNotification : NSObject

@property (strong, nonatomic, readonly) NSDate *date;
@property (nonatomic, readonly) BOOL isRead;
@property (strong, nonatomic, readonly) NSString *subject;
@property (nonatomic, readonly) long emailId;
@property (nonatomic, readonly) long mrId;
@property (strong, nonatomic, readonly) NSString *vName;
@property (nonatomic, readonly) long algId;
@property (strong, nonatomic, readonly) NSString *dateAsString;
@property (strong, nonatomic, readonly) NSString *alName;
@property (strong, nonatomic, readonly) NSString *type;
@property (strong, nonatomic, readonly) NSString *stringified;


- (id)initWithDictionary:(NSDictionary *)userInfo;
- (id)initWithValuesDate:(NSDate *)date isRead:(Boolean)isRead subject:(NSString *)subject emailId:(long)emailId mrId:(long)mrId viewName:(NSString *)vName algId:(long)algId alName:(NSString *)alName type:(NSString *)type;
- (id)initWithValuesLongLongDate:(long long)date isRead:(Boolean)isRead subject:(NSString *)subject emailId:(long)emailId mrId:(long)mrId viewName:(NSString *)vName algId:(long)algId alName:(NSString *)alName type:(NSString *)type;
- (id)initWithExampleValues;

@end

#endif
