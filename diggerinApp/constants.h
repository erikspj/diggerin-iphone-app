//
//  constants.h
//  diggerinApp
//
//  Created by Erik Spjelkavik on 15.04.15.
//  Copyright (c) 2015 diggerin. All rights reserved.
//

#ifndef diggerinApp_constants_h
#define diggerinApp_constants_h

#pragma mark Shared constants
static NSString * const kNotifications = @"notifications";
static NSString * const kOk = @"OK";

#pragma mark Shared keys
static NSString * const kAppNameKey = @"AppName";
static NSString * const kAuthenticationFailedKey = @"Authentication Failed";
static NSString * const kDataKey = @"Data";
static NSString * const kNetworkErrorKey = @"Network error";
static NSString * const kSharedTable = @"Shared";
static NSString * const kWrongUsernameOrPasswordKey = @"Wrong username or password";

#pragma mark ActivationViewController keys
static NSString * const kActivationErrorKey = @"Activation error";
static NSString * const kActivationErrorWithParameterKey = @"Activation error:";
static NSString * const kActivationFailedKey = @"Activation Failed";
static NSString * const kScalarResultKey = @"scalar_result";

#pragma mark AppDelegate keys
static NSString * const kActivatedKey = @"Activated";
static NSString * const kActivatedWithParameterKey = @"Activated:";
static NSString * const kDapiStatusKey = @"dapi_status";
static NSString * const kMainStoryboard = @"Main";
static NSString * const kMessage = @"Message";
static NSString * const kNotificationsViewId = @"notificationsView";
static NSString * const kRegisteringErrorKey = @"Registering error";
static NSString * const kRegisteringErrorWithParameterKey = @"Registering error:";

#pragma mark ViewController keys
static NSString * const kActivationKey = @"activation";
static NSString * const kNotificationFailedKey = @"Notification failed";
static NSString * const kNotificationFailedWithParameterKey = @"Notification failed: %@";
static NSString * const kDapiResultKey = @"dapi_result";
static NSString * const kExceptionKey = @"EXCEPTION";
static NSString * const kExceptionsWithParameterKey = @"%d exception(s)";
static NSString * const kIdenticalMessageKey = @"Identical message";
static NSString * const kMessageDiscardedKey = @"Message discarded";
static NSString * const kMessageProcessedKey = @"Message processed";
static NSString * const kMessagesQueuedWithParameterKey = @"%d message(s) queued";
static NSString * const kNotificationDiscardedKey = @"notification_discarded";
static NSString * const kQueueKey = @"QUEUE";

#pragma mark DiggerinNotification constants
static NSString * const kSpace = @" ";
static NSString * const kSubstitute = @"_";
static NSString * const kTestNotificationTable = @"testnotification";

#pragma mark DiggerinNotification keys
static NSString * const kDateKey = @"date";
static NSString * const kIsReadKey = @"eIsRead";
static NSString * const kSubjectKey = @"eSubject";
static NSString * const kEmailIdKey = @"emailId";
static NSString * const kMrIdKey = @"uMRId";
static NSString * const kViewNameKey = @"vName";
static NSString * const kAlgIdKey = @"algId";
static NSString * const kAlNameKey = @"alName";
static NSString * const kTypeKey = @"eType";

#pragma mark NotificationsViewController constants
static NSString * const kUserId = @"4FA808C6-881A-4845-96CE-BA61D1E13969"; // Mockup
static int const kCompanyId = 1;

#pragma mark NotificationsViewController keys
static NSString * const kEmailUriKey = @"EmailUri";
static NSString * const kTriggerImageKey = @"Gaming-Trigger-Mode-icon.png";
static NSString * const kTSimpleTableItemId = @"SimpleTableItem";
static NSString * const kTriggerUriKey = @"TriggerUri";
static NSString * const kMarkedAsReadTitleKey = @"Marked as read title";
static NSString * const kMarkedAsReadKey = @"Marked as read";
static NSString * const kMarkAsReadErrorKey = @"Mark as read error";
static NSString * const kMarkAsReadErrorWithParameterKey = @"Mark as read error: %@";

#endif
