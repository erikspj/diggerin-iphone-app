//
//  AppDelegate.m
//  diggerinApp
//
//  Created by Erik Spjelkavik on 03.10.14.
//  Copyright (c) 2014 diggerin. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@property (strong, nonatomic) NSMutableData *responseData;
@property (strong, nonatomic) KeychainItemWrapper *keychain;

@end

@implementation AppDelegate

#pragma mark Application Methods

// Called when the application is up and running
// Creates the public member credential from the keychain if the keychain contains username (kSecAttrAccount)
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if (!self.notifications)
    {
        self.notifications = [[NSMutableArray alloc] init];
    }
    
    if (!self.keychain)
    {
        self.keychain = [[KeychainItemWrapper alloc] initWithIdentifier:NSLocalizedStringFromTable(kAppNameKey, kSharedTable, nil)
                                                            accessGroup:nil];
        self.keychain[(__bridge id)kSecAttrAccessible] = (__bridge id)kSecAttrAccessibleWhenUnlocked;
    }
    
    if ([self.keychain[(__bridge id)kSecAttrAccount] length] > 0)
    {
        self.credential = [NSURLCredential credentialWithUser:self.keychain[(__bridge id)kSecAttrAccount]
                                                     password:self.keychain[(__bridge id)kSecValueData]
                                                  persistence:NSURLCredentialPersistenceForSession];
    }
    
    return YES;
}

// Called when the application becomes active
// Updates the badge on the application's icon with the number of notifications received
// Presents the NotificationsViewController if there exist received notifications
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = self.notifications.count;
    
    if (self.notifications.count > 0)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:kMainStoryboard bundle:nil];
        NotificationsViewController *nvc = [storyboard instantiateViewControllerWithIdentifier:kNotificationsViewId];
        [self.window.rootViewController presentViewController:nvc animated:NO completion:nil];
    }
}

#pragma mark Key Value Object Methods

// Called when the observed object changes
// Do not automatically notify if the NSArray notifications change  
+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key
{
    if ([key isEqualToString:kNotifications])
    {
        return NO;
    }
    else
    {
        return [super automaticallyNotifiesObserversForKey:key];
    }
}

- (id)objectInNotificationsAtIndex:(NSUInteger)index
{
    return self.notifications[index];
}

- (void)insertObject:(DiggerinNotification *)object inNotificationsAtIndex:(NSUInteger)index
{
    [self.notifications insertObject:object atIndex:index];
}

- (void)removeObjectFromNotificationsAtIndex:(NSUInteger)index
{
    [self.notifications removeObjectAtIndex:index];
}

#pragma mark Remote Notification Methods

// We are registered, to now store the device token (as a string) on the AppDelegate instance taking care to remove the angle brackets first
// Add the application to the Diggerin system with the device token
// Call the ViewController to show an error message if the connection fails
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSCharacterSet *angleBrackets = [NSCharacterSet characterSetWithCharactersInString:@"<>"];
    self.deviceToken = [[deviceToken description] stringByTrimmingCharactersInSet:angleBrackets];
    NSURLRequest *request = [DAPI getNotificationAddAppApp_name:NSLocalizedStringFromTable(kAppNameKey, kSharedTable, nil)
                                                             id:self.deviceToken];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    
    if (!connection)
    {
        [(ViewController *)self.window.rootViewController showAlertWithTitle:NSLocalizedString(kRegisteringErrorKey, nil)
                                                                  andMessage:[NSString stringWithFormat:NSLocalizedString(kRegisteringErrorWithParameterKey, nil),
                                                                                NSLocalizedString(kNetworkErrorKey, nil)]];
    }
}

// Handle any failure to register.
// Call the ViewController to show the error message.
// Set the deviceToken to an empty string to prevent the insert from failing.
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    [(ViewController *)self.window.rootViewController showAlertWithTitle:NSLocalizedString(kRegisteringErrorKey, nil)
                                                              andMessage:[NSString stringWithFormat:NSLocalizedString(kRegisteringErrorWithParameterKey, nil),
                                                                            error.userInfo[NSLocalizedDescriptionKey]]];
    
    self.deviceToken = @"";
}

#pragma mark NSURLConnection Delegate Methods

// Called when there is some sort of response from the DAPI
// Initializes responseData
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[NSMutableData alloc] init];
}

// Called when the object receives data
// Appends the received data to responseData
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [self.responseData appendData:data];
}

// Do not cache response
- (NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

// Called when the response stream has finished
// Check if the application was added successfully.
// Call the ViewController to show that the Diggerin notification system is activated.
- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSError *error;
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:self.responseData options:NSJSONReadingMutableLeaves error:&error];
    ViewController *viewController = (ViewController *)self.window.rootViewController;
    
    if ([kOk caseInsensitiveCompare:result[kDataKey][kDapiStatusKey]] == NSOrderedSame)
    {
        [viewController showAlertWithTitle:NSLocalizedString(kActivatedKey, nil)
                                andMessage:[NSString stringWithFormat:NSLocalizedString(kActivatedWithParameterKey, nil),
                                            self.keychain[(__bridge id)kSecAttrAccount]]];
    }
    else
    {
        [viewController showAlertWithTitle:NSLocalizedString(kRegisteringErrorKey, nil)
                                andMessage:[NSString stringWithFormat:NSLocalizedString(kRegisteringErrorWithParameterKey, nil),
                                            result[kMessage]]];
    }
}

// Called if the connection failed
// Calls the ViewController to show the user the error message
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [(ViewController *)self.window.rootViewController showAlertWithTitle:NSLocalizedString(kRegisteringErrorKey, nil)
                                                              andMessage:[NSString stringWithFormat:NSLocalizedString(kRegisteringErrorWithParameterKey, nil),
                                                                          error.userInfo[NSLocalizedDescriptionKey]]];
}

// Called if the DAPI requests an authentication
// Sends the credential member to the sender if the authentication succeedes.
// Calls the ViewController to show an error message if the authentication fails.
- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0)
    {
        [challenge.sender useCredential:self.credential forAuthenticationChallenge:challenge];
    }
    else
    {
        [(ViewController *)self.window.rootViewController showAlertWithTitle:NSLocalizedString(kAuthenticationFailedKey, nil)
                                                                  andMessage:NSLocalizedString(kWrongUsernameOrPasswordKey, nil)];
    }
}

#pragma mark Remote Notification Methods

// Called if a remote notification arrives.
// Creates a DiggerinNotification from the notification and adds it to the NSMutableArray notifications.
// Informs the observer (ViewController) if this is the first notification to arrive.
// Updates the badge on the application's icon with the number of notifications received.
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    DiggerinNotification *notification = [[DiggerinNotification alloc] initWithDictionary:userInfo];
    
    if (notification)
    {
        if (self.notifications.count == 0)
        {
            [self willChangeValueForKey:kNotifications];
        }
        
        [self.notifications addObject:notification];
        
        if (self.notifications.count == 1)
        {
            [self didChangeValueForKey:kNotifications];
        }
    }
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = self.notifications.count;
}

#pragma mark Activation Methods

// Called by the ActivationViewController via the ViewController
// Stores the user and password values of the credential in the keychain if stayLoggedIn = YES.
// Removes the credential from the keychain if stayLoggedIn = NO.
- (void)activateWithCredential:(NSURLCredential *)credential stayLoggedIn:(Boolean)stayLoggedIn
{
    self.credential = credential;
    
    if (stayLoggedIn)
    {
        self.keychain[(__bridge id)kSecAttrAccount] = credential.user;
        self.keychain[(__bridge id)kSecValueData] = credential.password;
    }
    else
    {
        [self.keychain resetKeychainItem];
    }
    
}

#pragma mark Getter Methods
- (Boolean)isLoggedIn
{
    return !!self.credential;
}

- (Boolean)hasNotifications
{
    return self.notifications.count > 0;
}
@end
