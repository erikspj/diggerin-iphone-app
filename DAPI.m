/*
 DAPI iOS Library
 
 See http://dapi.diggerin.com/
 
 Copyright (C) 2015 DiggerIn
 
 Automatically generated from DAPI source-code 2015-02-26 10:19
 
 Licensing terms not decided as of February 2015 (A permissive license is most probable).
 Please enquire as necessary to bef_diggerin@diggerin.com
 */
#import "DAPI.h"

static NSString* SERVER_URL = @"http://dapi.diggerin.com";// @"http://dapi.diggerin.com/"

@implementation DAPI

+(void)setServerAddress:(NSString*)serverAddress{
    SERVER_URL = serverAddress;
}

+(NSString*)getServerAddress{
    return SERVER_URL;
}

+(NSString *)urlEncodeValue:(NSString *)sourceString
{
    NSString *urlEncoded =
    (__bridge_transfer NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(__bridge CFStringRef)sourceString,NULL,
                                                                          (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
                                                                          kCFStringEncodingUTF8);
    return urlEncoded;
}


// Generate full url link for request
+(NSString*)generateFullURLforApi:(NSString*)apiURL
{
    NSString *url = [NSString stringWithFormat:@"%@/api/%@",SERVER_URL,apiURL];
    return url;
}

+(NSMutableURLRequest*)generateGETRequestForURL:(NSString*)url
{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    return request;
}

+(NSMutableURLRequest*)generatePOSTRequestForURL:(NSString*)url data:(NSData*)data
{
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[data length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:data];
    
    return request;
}


//******
//  API
//******



+(NSMutableURLRequest*) getSummaryId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Summary/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getVerifyCredentials;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"VerifyCredentials"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getVerifyCredentialsClient_id: (NSString*)client_id;
{
    client_id = [DAPI urlEncodeValue:client_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"VerifyCredentials/",  client_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getVerifyPasswordEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"VerifyPassword/",  email, @"/",  password, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postVerifyPasswordEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* body = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"VerifyPassword/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getChangePasswordEmail: (NSString*)email old_password: (NSString*)old_password new_password: (NSString*)new_password;
{
    email = [DAPI urlEncodeValue:email];
    old_password = [DAPI urlEncodeValue:old_password];
    new_password = [DAPI urlEncodeValue:new_password];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"ChangePassword/",  email, @"/",  old_password, @"/",  new_password, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postChangePasswordEmail: (NSString*)email old_password: (NSString*)old_password new_password: (NSString*)new_password;
{
    email = [DAPI urlEncodeValue:email];
    old_password = [DAPI urlEncodeValue:old_password];
    new_password = [DAPI urlEncodeValue:new_password];
    NSString* body = [NSString stringWithFormat:@"email=%@&old_password=%@&new_password=%@", email, old_password, new_password];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"ChangePassword/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getResetPasswordEmail: (NSString*)email;
{
    email = [DAPI urlEncodeValue:email];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"ResetPassword/",  email, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getResetPasswordEmail: (NSString*)email execute_id: (NSString*)execute_id;
{
    email = [DAPI urlEncodeValue:email];
    execute_id = [DAPI urlEncodeValue:execute_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"ResetPassword/",  email, @"/",  execute_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getTranslationsVersionClient_id: (NSString*)client_id language: (NSString*)language;
{
    client_id = [DAPI urlEncodeValue:client_id];
    language = [DAPI urlEncodeValue:language];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Translations/Version/",  client_id, @"/",  language, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getTranslationsClient_id: (NSString*)client_id language: (NSString*)language keys: (NSString*)keys;
{
    client_id = [DAPI urlEncodeValue:client_id];
    language = [DAPI urlEncodeValue:language];
    keys = [DAPI urlEncodeValue:keys];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Translations/",  client_id, @"/",  language, @"/",  keys, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postTranslationsClient_id: (NSString*)client_id language: (NSString*)language keys: (NSString*)keys;
{
    client_id = [DAPI urlEncodeValue:client_id];
    language = [DAPI urlEncodeValue:language];
    keys = [DAPI urlEncodeValue:keys];
    NSString* body = [NSString stringWithFormat:@"client_id=%@&language=%@&keys=%@", client_id, language, keys];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Translations/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getDocumentationRefresh;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Documentation/Refresh"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentAddComponent_type: (NSString*)component_type registration_id: (NSString*)registration_id;
{
    component_type = [DAPI urlEncodeValue:component_type];
    registration_id = [DAPI urlEncodeValue:registration_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/Add/",  component_type, @"/",  registration_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAddComponent_type: (NSString*)component_type registration_id: (NSString*)registration_id;
{
    component_type = [DAPI urlEncodeValue:component_type];
    registration_id = [DAPI urlEncodeValue:registration_id];
    NSString* body = [NSString stringWithFormat:@"component_type=%@&registration_id=%@", component_type, registration_id];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component/Add/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getComponentAddComponent_type: (NSString*)component_type;
{
    component_type = [DAPI urlEncodeValue:component_type];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/Add/",  component_type, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAddComponent_type: (NSString*)component_type;
{
    component_type = [DAPI urlEncodeValue:component_type];
    NSString* body = [NSString stringWithFormat:@"component_type=%@", component_type];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component/Add/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getComponentAtTime: (NSString*)time script: (NSString*)script;
{
    time = [DAPI urlEncodeValue:time];
    script = [DAPI urlEncodeValue:script];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/At/",  time, @"/",  script, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAtTime: (NSString*)time script: (NSString*)script;
{
    time = [DAPI urlEncodeValue:time];
    script = [DAPI urlEncodeValue:script];
    NSString* body = [NSString stringWithFormat:@"time=%@&script=%@", time, script];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component/At/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getComponentPropertyId: (NSString*)id name: (NSString*)name;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/",  id, @"/Property/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getComponentPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    field = [DAPI urlEncodeValue:field];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Component/",  id, @"/Property/",  name, @"/",  field, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentRepresentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/",  id, @"/Represent/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Component/",  id, @"/AddProperty/",  property_name, @"/",  value, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* body = [NSString stringWithFormat:@"id=%@&property_name=%@&value=%@", id, property_name, value];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component//AddProperty/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getComponentRunId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/",  id, @"/Run/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentAssignRegistration_id: (NSString*)registration_id person_id: (NSString*)person_id;
{
    registration_id = [DAPI urlEncodeValue:registration_id];
    person_id = [DAPI urlEncodeValue:person_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/",  registration_id, @"/Assign/",  person_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAssignRegistration_id: (NSString*)registration_id person_id: (NSString*)person_id;
{
    registration_id = [DAPI urlEncodeValue:registration_id];
    person_id = [DAPI urlEncodeValue:person_id];
    NSString* body = [NSString stringWithFormat:@"registration_id=%@&person_id=%@", registration_id, person_id];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component//Assign/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getComponentHistory;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Component//History"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getComponentHistoryId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/",  id, @"/History/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getComponentHistoryId: (NSString*)id limit: (NSString*)limit;
{
    id = [DAPI urlEncodeValue:id];
    limit = [DAPI urlEncodeValue:limit];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/",  id, @"/History/",  limit, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentListenerLog;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Component/ListenerLog"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getComponentListenerLogId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/",  id, @"/ListenerLog/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponentSalesforceAgentRefresh;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Component/SalesforceAgent/Refresh"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getComponent;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Component"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getComponentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Component/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPersonLogout;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Person/Logout"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonLogout;
{
    NSString* body = [NSString stringWithFormat:@""];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/Logout/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonAddEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/Add/",  email, @"/",  password, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* body = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/Add/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonAddAsChildEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/AddAsChild/",  email, @"/",  password, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddAsChildEmail: (NSString*)email password: (NSString*)password;
{
    email = [DAPI urlEncodeValue:email];
    password = [DAPI urlEncodeValue:password];
    NSString* body = [NSString stringWithFormat:@"email=%@&password=%@", email, password];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/AddAsChild/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getPersonAddAsChildName: (NSString*)name;
{
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/AddAsChild/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddAsChildName: (NSString*)name;
{
    name = [DAPI urlEncodeValue:name];
    NSString* body = [NSString stringWithFormat:@"name=%@", name];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/AddAsChild/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonPropertyId: (NSString*)id name: (NSString*)name;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/",  id, @"/Property/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPersonPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    field = [DAPI urlEncodeValue:field];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Person/",  id, @"/Property/",  name, @"/",  field, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPersonRepresentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/",  id, @"/Represent/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPersonAddPropertyProperty_name: (NSString*)property_name value: (NSString*)value;
{
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/AddProperty/",  property_name, @"/",  value, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddPropertyProperty_name: (NSString*)property_name value: (NSString*)value;
{
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* body = [NSString stringWithFormat:@"property_name=%@&value=%@", property_name, value];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/AddProperty/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getPersonAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Person/",  id, @"/AddProperty/",  property_name, @"/",  value, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* body = [NSString stringWithFormat:@"id=%@&property_name=%@&value=%@", id, property_name, value];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person//AddProperty/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonAddChildChild: (NSString*)child;
{
    child = [DAPI urlEncodeValue:child];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/AddChild/",  child, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddChildChild: (NSString*)child;
{
    child = [DAPI urlEncodeValue:child];
    NSString* body = [NSString stringWithFormat:@"child=%@", child];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/AddChild/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getPersonAddChildParent: (NSString*)parent child: (NSString*)child;
{
    parent = [DAPI urlEncodeValue:parent];
    child = [DAPI urlEncodeValue:child];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/",  parent, @"/AddChild/",  child, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonAddChildParent: (NSString*)parent child: (NSString*)child;
{
    parent = [DAPI urlEncodeValue:parent];
    child = [DAPI urlEncodeValue:child];
    NSString* body = [NSString stringWithFormat:@"parent=%@&child=%@", parent, child];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person//AddChild/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient;
{
    recipient = [DAPI urlEncodeValue:recipient];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/DelegateRight/",  recipient, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient;
{
    recipient = [DAPI urlEncodeValue:recipient];
    NSString* body = [NSString stringWithFormat:@"recipient=%@", recipient];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/DelegateRight/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right;
{
    recipient = [DAPI urlEncodeValue:recipient];
    right = [DAPI urlEncodeValue:right];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/DelegateRight/",  recipient, @"/",  right, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right;
{
    recipient = [DAPI urlEncodeValue:recipient];
    right = [DAPI urlEncodeValue:right];
    NSString* body = [NSString stringWithFormat:@"recipient=%@&right=%@", recipient, right];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/DelegateRight/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right delegator: (NSString*)delegator;
{
    recipient = [DAPI urlEncodeValue:recipient];
    right = [DAPI urlEncodeValue:right];
    delegator = [DAPI urlEncodeValue:delegator];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Person/DelegateRight/",  recipient, @"/",  right, @"/",  delegator, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPersonDelegateRightRecipient: (NSString*)recipient right: (NSString*)right delegator: (NSString*)delegator;
{
    recipient = [DAPI urlEncodeValue:recipient];
    right = [DAPI urlEncodeValue:right];
    delegator = [DAPI urlEncodeValue:delegator];
    NSString* body = [NSString stringWithFormat:@"recipient=%@&right=%@&delegator=%@", recipient, right, delegator];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Person/DelegateRight/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPersonHistory;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"Person//History"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPersonHistoryId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/",  id, @"/History/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPersonHistoryId: (NSString*)id limit: (NSString*)limit;
{
    id = [DAPI urlEncodeValue:id];
    limit = [DAPI urlEncodeValue:limit];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/",  id, @"/History/",  limit, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPersonId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Person/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPersonId: (NSString*)id extend_rights: (NSString*)extend_rights;
{
    id = [DAPI urlEncodeValue:id];
    extend_rights = [DAPI urlEncodeValue:extend_rights];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Person/",  id, @"/",  extend_rights, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getNotificationAddEMailEmail: (NSString*)email;
{
    email = [DAPI urlEncodeValue:email];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Notification/AddEMail/",  email, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotificationAddEMailEmail: (NSString*)email;
{
    email = [DAPI urlEncodeValue:email];
    NSString* body = [NSString stringWithFormat:@"email=%@", email];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notification/AddEMail/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getNotificationAddEMailEmail: (NSString*)email name: (NSString*)name;
{
    email = [DAPI urlEncodeValue:email];
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Notification/AddEMail/",  email, @"/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotificationAddEMailEmail: (NSString*)email name: (NSString*)name;
{
    email = [DAPI urlEncodeValue:email];
    name = [DAPI urlEncodeValue:name];
    NSString* body = [NSString stringWithFormat:@"email=%@&name=%@", email, name];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notification/AddEMail/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getNotificationAddCellularNumber: (NSString*)number;
{
    number = [DAPI urlEncodeValue:number];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Notification/AddCellular/",  number, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotificationAddCellularNumber: (NSString*)number;
{
    number = [DAPI urlEncodeValue:number];
    NSString* body = [NSString stringWithFormat:@"number=%@", number];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notification/AddCellular/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getNotificationAddCellularNumber: (NSString*)number name: (NSString*)name;
{
    number = [DAPI urlEncodeValue:number];
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Notification/AddCellular/",  number, @"/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotificationAddCellularNumber: (NSString*)number name: (NSString*)name;
{
    number = [DAPI urlEncodeValue:number];
    name = [DAPI urlEncodeValue:name];
    NSString* body = [NSString stringWithFormat:@"number=%@&name=%@", number, name];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notification/AddCellular/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getNotificationAddAppApp_name: (NSString*)app_name id: (NSString*)id;
{
    app_name = [DAPI urlEncodeValue:app_name];
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Notification/AddApp/",  app_name, @"/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotificationAddAppApp_name: (NSString*)app_name id: (NSString*)id;
{
    app_name = [DAPI urlEncodeValue:app_name];
    id = [DAPI urlEncodeValue:id];
    NSString* body = [NSString stringWithFormat:@"app_name=%@&id=%@", app_name, id];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notification/AddApp/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getNotifyText: (NSString*)text;
{
    text = [DAPI urlEncodeValue:text];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Notify/",  text, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotifyText: (NSString*)text;
{
    text = [DAPI urlEncodeValue:text];
    NSString* body = [NSString stringWithFormat:@"text=%@", text];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notify/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getNotifyText: (NSString*)text time_filter: (NSString*)time_filter;
{
    text = [DAPI urlEncodeValue:text];
    time_filter = [DAPI urlEncodeValue:time_filter];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Notify/",  text, @"/",  time_filter, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postNotifyText: (NSString*)text time_filter: (NSString*)time_filter;
{
    text = [DAPI urlEncodeValue:text];
    time_filter = [DAPI urlEncodeValue:time_filter];
    NSString* body = [NSString stringWithFormat:@"text=%@&time_filter=%@", text, time_filter];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Notify/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getComponentAddPartId: (NSString*)id part_type: (NSString*)part_type;
{
    id = [DAPI urlEncodeValue:id];
    part_type = [DAPI urlEncodeValue:part_type];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Component/",  id, @"/AddPart/",  part_type, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAddPartId: (NSString*)id part_type: (NSString*)part_type;
{
    id = [DAPI urlEncodeValue:id];
    part_type = [DAPI urlEncodeValue:part_type];
    NSString* body = [NSString stringWithFormat:@"id=%@&part_type=%@", id, part_type];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component//AddPart/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}


+(NSMutableURLRequest*) getComponentAddPartId: (NSString*)id part_type: (NSString*)part_type parent_part_id: (NSString*)parent_part_id;
{
    id = [DAPI urlEncodeValue:id];
    part_type = [DAPI urlEncodeValue:part_type];
    parent_part_id = [DAPI urlEncodeValue:parent_part_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Component/",  id, @"/AddPart/",  part_type, @"/",  parent_part_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postComponentAddPartId: (NSString*)id part_type: (NSString*)part_type parent_part_id: (NSString*)parent_part_id;
{
    id = [DAPI urlEncodeValue:id];
    part_type = [DAPI urlEncodeValue:part_type];
    parent_part_id = [DAPI urlEncodeValue:parent_part_id];
    NSString* body = [NSString stringWithFormat:@"id=%@&part_type=%@&parent_part_id=%@", id, part_type, parent_part_id];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Component//AddPart/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPartAggregationCurrentId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    aggregation_type = [DAPI urlEncodeValue:aggregation_type];
    period = [DAPI urlEncodeValue:period];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/",  aggregation_type, @"/",  period, @"/Current/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartAggregationLastId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    aggregation_type = [DAPI urlEncodeValue:aggregation_type];
    period = [DAPI urlEncodeValue:period];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/",  aggregation_type, @"/",  period, @"/Last/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartAggregationHistoryId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    aggregation_type = [DAPI urlEncodeValue:aggregation_type];
    period = [DAPI urlEncodeValue:period];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/",  aggregation_type, @"/",  period, @"/History/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property period: (NSString*)period;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    period = [DAPI urlEncodeValue:period];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/",  period, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPartAggregationId: (NSString*)id part_property: (NSString*)part_property aggregation_type: (NSString*)aggregation_type period: (NSString*)period;
{
    id = [DAPI urlEncodeValue:id];
    part_property = [DAPI urlEncodeValue:part_property];
    aggregation_type = [DAPI urlEncodeValue:aggregation_type];
    period = [DAPI urlEncodeValue:period];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@", @"Part/",  id, @"/Aggregation/",  part_property, @"/",  aggregation_type, @"/",  period, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getAggregationId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Aggregation/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartAtId: (NSString*)id time: (NSString*)time script: (NSString*)script;
{
    id = [DAPI urlEncodeValue:id];
    time = [DAPI urlEncodeValue:time];
    script = [DAPI urlEncodeValue:script];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Part/",  id, @"/At/",  time, @"/",  script, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPartAtId: (NSString*)id time: (NSString*)time script: (NSString*)script;
{
    id = [DAPI urlEncodeValue:id];
    time = [DAPI urlEncodeValue:time];
    script = [DAPI urlEncodeValue:script];
    NSString* body = [NSString stringWithFormat:@"id=%@&time=%@&script=%@", id, time, script];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Part//At/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPartPropertyId: (NSString*)id name: (NSString*)name;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Part/",  id, @"/Property/",  name, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPartPropertyId: (NSString*)id name: (NSString*)name field: (NSString*)field;
{
    id = [DAPI urlEncodeValue:id];
    name = [DAPI urlEncodeValue:name];
    field = [DAPI urlEncodeValue:field];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Part/",  id, @"/Property/",  name, @"/",  field, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartRepresentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Part/",  id, @"/Represent/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Part/",  id, @"/AddProperty/",  property_name, @"/",  value, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPartAddPropertyId: (NSString*)id property_name: (NSString*)property_name value: (NSString*)value;
{
    id = [DAPI urlEncodeValue:id];
    property_name = [DAPI urlEncodeValue:property_name];
    value = [DAPI urlEncodeValue:value];
    NSString* body = [NSString stringWithFormat:@"id=%@&property_name=%@&value=%@", id, property_name, value];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Part//AddProperty/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPartHistoryId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Part/",  id, @"/History/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPartHistoryId: (NSString*)id limit: (NSString*)limit;
{
    id = [DAPI urlEncodeValue:id];
    limit = [DAPI urlEncodeValue:limit];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Part/",  id, @"/History/",  limit, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartRefreshId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Part/",  id, @"/Refresh/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPartId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Part/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPartId: (NSString*)id component_id: (NSString*)component_id;
{
    id = [DAPI urlEncodeValue:id];
    component_id = [DAPI urlEncodeValue:component_id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Part/",  id, @"/",  component_id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Property/",  id, @"/History/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id limit: (NSString*)limit no_filter: (NSString*)no_filter;
{
    id = [DAPI urlEncodeValue:id];
    limit = [DAPI urlEncodeValue:limit];
    no_filter = [DAPI urlEncodeValue:no_filter];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@%@%@", @"Property/",  id, @"/History/",  limit, @"/",  no_filter, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getPropertyHistoryId: (NSString*)id limit: (NSString*)limit;
{
    id = [DAPI urlEncodeValue:id];
    limit = [DAPI urlEncodeValue:limit];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"Property/",  id, @"/History/",  limit, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPropertySetNoLongerCurrentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Property/",  id, @"/SetNoLongerCurrent/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}

+(NSMutableURLRequest*) postPropertySetNoLongerCurrentId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* body = [NSString stringWithFormat:@"id=%@", id];
    NSData *postData = [body dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString* url = @"Property//SetNoLongerCurrent/";
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generatePOSTRequestForURL:url data:postData];
}




+(NSMutableURLRequest*) getPropertyRunId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Property/",  id, @"/Run/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getPropertyId: (NSString*)id;
{
    id = [DAPI urlEncodeValue:id];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"Property/",  id, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getHTTPStatus;
{
    NSString* url = [NSString stringWithFormat:@"%@", @"HTTPStatus"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getHTTPStatusStatus_code: (NSString*)status_code;
{
    status_code = [DAPI urlEncodeValue:status_code];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"HTTPStatus/",  status_code, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}


+(NSMutableURLRequest*) getHTTPStatusStatus_code: (NSString*)status_code message: (NSString*)message;
{
    status_code = [DAPI urlEncodeValue:status_code];
    message = [DAPI urlEncodeValue:message];
    NSString* url = [NSString stringWithFormat:@"%@%@%@%@%@", @"HTTPStatus/",  status_code, @"/",  message, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}




+(NSMutableURLRequest*) getExceptionDetailsSub_system: (NSString*)sub_system;
{
    sub_system = [DAPI urlEncodeValue:sub_system];
    NSString* url = [NSString stringWithFormat:@"%@%@%@", @"ExceptionDetails/",  sub_system, @"/"];
    url = [DAPI generateFullURLforApi: url];
    return [DAPI generateGETRequestForURL:url];
}



@end